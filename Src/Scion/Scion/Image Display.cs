﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Scion.Library.Underlying;

namespace Scion
{
    /// <summary>
    /// This class basically displays the given buffered image passed into the ImageDisplay contructor.
    /// </summary>
    public partial class ImageDisplay : Form
    {
        BufferedImage bmp;

        public ImageDisplay(BufferedImage c)
        {
            bmp = c;
            InitializeComponent();
        }

        private void ImageDisplay_Load_1(object sender, EventArgs e)
        {
            pictureBox1.Size = new Size(bmp.image.Width, bmp.imageHeight);
            pictureBox1.Location = new Point(0, 0);
            pictureBox1.Image = bmp.image;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "JPG File (*.jpg)|*.jpg|BMP File (*.bmp)|*.bmp";
                DialogResult dr = sfd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    if (sfd.FilterIndex == 0)
                    {
                        bmp.image.Save(sfd.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    else
                    {
                        bmp.image.Save(sfd.FileName, System.Drawing.Imaging.ImageFormat.Bmp);
                    }
                }
                sfd.Dispose();
                GC.Collect();
            }
            catch
            {
            }
        }
    }
}
