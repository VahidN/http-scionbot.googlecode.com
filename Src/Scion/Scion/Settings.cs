﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Scion
{
    public partial class Settings : Form
    {
        /// <summary>
        /// The Settings form.
        /// </summary>
        public Settings()
        {
            InitializeComponent();
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            textBox1.Text = Scion.Misc.ScionSettings.RefreshInterval.ToString();
            textBox2.Text = Scion.Misc.ScionSettings.JavaDirectory;
            checkBox1.Checked = Scion.Misc.ScionSettings.EnableAeroStartup;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string directory = Scion.Misc.ScionSettings.FindJavaDirectory();
            switch (directory)
            {
                case "NO JAVA":
                    MessageBox.Show("Your Java directory could not be located!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case "NO JRE":
                    MessageBox.Show("Your JRE directory could not be located!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }
            DialogResult r = MessageBox.Show("Is " + directory + " correct?", "Correct", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (r == DialogResult.Yes)
            {
                textBox2.Text = directory;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string newStr = "";
            try
            {
                string str = textBox2.Text;
                newStr = str.Remove(0, str.LastIndexOf(@"\"));
            }
            catch
            {
            }
            if (newStr.IndexOf("bin") == -1)
            {
                MessageBox.Show(@"You have specified an incorrect Java directory! Please remember to include the bin directory. Ex: C:\...\Java\jre1.6.0_04\bin", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int interval;
            try
            {
                interval = int.Parse(textBox1.Text);
                Scion.Misc.ScionSettings.RefreshInterval = interval;
            }
            catch
            {
                MessageBox.Show("Failed to save the Refresh Interval field.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            DirectoryInfo javaDR = new DirectoryInfo(textBox2.Text);
            if (javaDR.Exists)
            {
                Scion.Misc.ScionSettings.JavaDirectory = textBox2.Text;
            }
            else
            {
                MessageBox.Show("Failed to save the Java Directory field.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Scion.Misc.ScionSettings.EnableAeroStartup = checkBox1.Checked;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
