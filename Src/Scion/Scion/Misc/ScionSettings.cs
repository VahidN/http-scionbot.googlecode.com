﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Scion.External;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Diagnostics;

namespace Scion.Misc
{
    /// <summary>
    /// Methods used for Scion's settings.
    /// </summary>
    public class ScionSettings
    {
        /// <summary>
        /// Scion has problems with users running on Aero. Disable Aero effects while the bot is running.
        /// Aero is turned back on when the Scion process terminates.
        /// </summary>
        public static void DisableAero()
        {
            try
            {
                if (API.DwmIsCompositionEnabled())
                {
                    API.Win32DwmEnableComposition((uint)WM.M.DWM_EC_DISABLECOMPOSITION);
                }
            }
            catch
            {
                //No aero
            }
        }

        /// <summary>
        /// If users want, they can run the bot with Windows Aero enabled.
        /// </summary>
        public static bool EnableAeroStartup
        {
            get
            {
                return Scion.Properties.Settings.Default.EnableAero;
            }
            set
            {
                Scion.Properties.Settings.Default.EnableAero = value;
                Scion.Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// Get/set our refresh interval. This is instantly applied and actions will take effect in all bots.
        /// </summary>
        public static int RefreshInterval
        {
            get
            {
                return Scion.Properties.Settings.Default.RefreshInterval;
            }
            set
            {
                Scion.Properties.Settings.Default.RefreshInterval = value;
                Scion.Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// Get/set the java directory.
        /// </summary>
        public static string JavaDirectory
        {
            get
            {
                return Scion.Properties.Settings.Default.JavaDirectory;
            }
            set
            {
                Scion.Properties.Settings.Default.JavaDirectory = value;
                Scion.Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// Moves Runescape.jar to the cache.
        /// </summary>
        public static void MoveToCache()
        {
            string path = @"C:\WINDOWS\.jagex_cache_32\runescape";
            DirectoryInfo di = new DirectoryInfo(path);
            if (di.Exists)
            {
                FileInfo fi = new FileInfo(StartupPath + @"\runescape.jar");
                if (fi.Exists)
                {
                    FileInfo rsCache = new FileInfo(di + @"\runescape.jar");
                    if (rsCache.Exists)
                        rsCache.Delete();
                    fi.CopyTo(di + @"\runescape.jar");
                }
            }
        }

        /// <summary>
        /// If you're running Windows 7 and not running Scion in compatibily mode, this will notify the user.
        /// </summary>
        /// <returns>Returns false if you're using Windows 7 and the registry key is not found.</returns>
        public static bool CheckCompatibility()
        {
            if (System.Environment.OSVersion.VersionString.IndexOf("6.1") > -1)
            {
                RegistryKey machine = Registry.CurrentUser;
                machine = machine.OpenSubKey(@"Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers", true);
                string app = Application.ExecutablePath.Remove(0, Application.ExecutablePath.IndexOf(@"\") + @"\".Length);
                if (machine == null)
                    return false;
                foreach (string str in machine.GetValueNames())
                    if (str.ToLower().IndexOf(app.ToLower()) > -1)
                        return true;
                return false;
            }
            else return true;
        }

        /// <summary>
        /// Gets the startup path for Scion.exe.
        /// </summary>
        public static string StartupPath
        {
            get
            {
                return System.Windows.Forms.Application.StartupPath;
            }
        }

        /// <summary>
        /// Figures out if Scion is using the latest JRE.
        /// </summary>
        /// <param name="path">The path to check.</param>
        /// <returns>Returns true if Scion is using the old JRE.</returns>
        public static bool IsOldJava(string path)
        {
            if ((path.IndexOf("jre") == -1) && (path.IndexOf("_") > -1))
                return true;
            return false;
        }

        /// <summary>
        /// Find the Java Directory. javaPaths may be edited if you have a different install location for Java.
        /// Otherwise, you'll have to manually edit your path in the Settings Form at runtime.
        /// </summary>
        /// <returns>Returns the Java directory.</returns>
        public static string FindJavaDirectory()
        {
            string[] javaPaths = { @"C:\Program Files\Java", @"C:\Program Files (x86)\Java" };
            DirectoryInfo javaPath = null;
            DirectoryInfo jrePath = null;
            foreach (string path in javaPaths)
            {
                DirectoryInfo c = new DirectoryInfo(path);
                if (c.Exists)
                {
                    javaPath = c;
                    break;
                }
            }
            if (javaPath == null)
                return "NO JAVA";
            foreach (DirectoryInfo d in javaPath.GetDirectories())
            {
                if (d.Name.IndexOf("jre6") > -1)
                {
                    jrePath = d;
                    break;
                }
            }
            if (jrePath == null)
                return "NO JRE";
            else
                return jrePath.FullName + @"\bin";
        }

        /// <summary>
        /// Kills all the running bots if the bot failed to kill them on the last shutdown.
        /// </summary>
        public static void KillRunningBots()
        {
            Process[] processes = Process.GetProcessesByName("java");
            foreach (Process p in processes)
                if (p.MainWindowTitle.IndexOf("Bot") > -1)
                        p.Kill();
        }
    }
}
