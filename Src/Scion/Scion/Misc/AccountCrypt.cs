﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scion.Misc
{
    /// <summary>
    /// Methods used to decrpyt/encrpyt for Account Manager.
    /// </summary>
    public class AccountCrypt
    {
        public static string Encrypt(string data)
        {
            return RijndaelSimple.Encrypt(data, CryptographySettings.passPhrase, CryptographySettings.saltValue,
                                                                CryptographySettings.hashAlgorithm, CryptographySettings.passwordIterations,
                                                                CryptographySettings.initVector, CryptographySettings.keySize);
        }

        public static string Decrypt(string data)
        {
            return RijndaelSimple.Decrypt(data, CryptographySettings.passPhrase, CryptographySettings.saltValue,
                                                                    CryptographySettings.hashAlgorithm, CryptographySettings.passwordIterations,
                                                                    CryptographySettings.initVector, CryptographySettings.keySize);
        }

        public static string DecryptUsername(string data)
        {
            string str = Decrypt(data);
            str = str.Remove(str.IndexOf(":"));
            str.Trim();
            return str;
        }

        public static string DecryptPassword(string data)
        {
            string str = Decrypt(data);
            str = str.Remove(0, str.IndexOf(":") + ":".Length);
            str.Trim();
            return str;
        }
    }
}
