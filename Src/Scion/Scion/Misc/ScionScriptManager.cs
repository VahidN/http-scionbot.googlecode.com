﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Scion.Misc
{
    /// <summary>
    /// Functions used to cluster scripts.
    /// </summary>
    public class ScionScriptManager
    {
        public static List<FileInfo> GetScripts()
        {
            string stringDirectory = ScionSettings.StartupPath + @"\Scripts";
            DirectoryInfo directory = new DirectoryInfo(stringDirectory);
            if (!directory.Exists)
            {
                return null;
            }
            List<FileInfo> files = new List<FileInfo>();
            foreach (FileInfo file in directory.GetFiles())
            {
                if (file.Extension.IndexOf("dll") > -1)
                {
                    files.Add(file);
                }
            }
            return files;
        }
    }
}
