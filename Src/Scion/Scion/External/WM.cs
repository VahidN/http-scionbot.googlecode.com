﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scion.External
{
    //Stores our cluster of WM/SWP message codes.
    class WM
    {
        //Enum M
        public enum M
        {
            WS_EX_NOACTIVATE = 0x8000000,
            WS_EX_LAYERED = 0x80000,
            LWA_ALPHA = 0x2,
            WM_PAINT = 0x000F,
            GWL_EXSTYLE = (-20),
            WS_EX_TOOLWINDOW = 0x80,
            WS_EX_APPWINDOW = 0x40000,
            GWL_STYLE = -16,
            WM_KILLFOCUS = 0x0008,
            WM_SETFOCUS = 0x0007,
            GW_HWNDPREV = 3,
            GW_HWNDNEXT = 2,
            DWM_EC_DISABLECOMPOSITION = 0,
            DWM_EC_ENABLECOMPOSITION = 1,
        }
        //Enum SWP
        public enum SWP
        {
            SWP_NOSIZE = 0x0001,
            SWP_NOMOVE = 0x0002,
            SWP_NOZORDER = 0x0004,
            SWP_NOREDRAW = 0x0008,
            SWP_NOACTIVATE = 0x0010,
            SWP_FRAMECHANGED = 0x0020,
            SWP_SHOWWINDOW = 0x0040,
            SWP_HIDEWINDOW = 0x0080,
            SWP_NOCOPYBITS = 0x0100,
            SWP_NOOWNERZORDER = 0x0200,
            SWP_NOSENDCHANGING = 0x0400,
        }
    }
}
