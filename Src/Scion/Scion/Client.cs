﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Scion.Misc;
using Scion.Library.Underlying;
using Scion.Library;
using Scion.Library.Advanced;
using Scion.Library.RS.UI;

namespace Scion
{
    /// <summary>
    /// The MDI form you see when you first run Scion.exe
    /// </summary>
    public partial class Client : Form
    {
        /// <summary>
        /// The list of all the Bots is located in this generic list.
        /// </summary>
        public List<Bot> BotList = new List<Bot>();

        public Client()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!ScionSettings.CheckCompatibility())
            {
                MessageBox.Show("You're running Windows 7, please goto Scion.exe's properties and set the compatibility mode to Windows 2000.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
                return;
            }
            ScionSettings.KillRunningBots();
            if (ScionSettings.JavaDirectory == "null")
            {
                MessageBox.Show("Welcome to Scion! The free multi-thread bot, brought to you by ScionBot.com.\n\n" +
                    "Please remember that Scion is to be used as an education tool only! The author (Jaco) is not held " +
                    "liable for the actions the end-user performs.", "Welcome!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                string javaDirectory = ScionSettings.FindJavaDirectory();
                switch (javaDirectory)
                {
                    case "NO JAVA":
                        MessageBox.Show("Please install the Java JRE and restart this bot.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "NO JRE":
                        MessageBox.Show("A directory for Java was found, but no JRE is installed. Please install the Java JRE and restart this bot.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    default:
                        DialogResult r = MessageBox.Show("Is " + javaDirectory + " correct?", "Correct", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (r == DialogResult.Yes)
                            ScionSettings.JavaDirectory = javaDirectory;
                        break;
                }
                MessageBox.Show("Before you run a bot in Scion, you must register an account in Account Manager.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            if (ScionSettings.JavaDirectory == "null")
                MessageBox.Show("You must adjust the Java Directory manually in Scion's Settings before running a bot.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else if (ScionSettings.IsOldJava(ScionSettings.JavaDirectory))
            {
                string javaDirectory = ScionSettings.FindJavaDirectory();
                DialogResult r = MessageBox.Show("You're using the older JRE. Is " + javaDirectory + " correct?", "Correct", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                    ScionSettings.JavaDirectory = javaDirectory;
                else
                    MessageBox.Show("You must adjust the Java Directory manually in Scion's Settings before running a bot.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            textBox1.BackColor = SystemColors.Window;
            if (!Scion.Misc.ScionSettings.EnableAeroStartup)
                ScionSettings.DisableAero();
            Fonts.LoadAllFonts();
            TopText.PopulateCommon();
            ChooseOption.DecryptImages();
            this.SizeChanged += new EventHandler(Client_SizeChanged);
            textBox1.GotFocus += new EventHandler(textBox1_GotFocus);
            textBox1.LostFocus += new EventHandler(textBox1_LostFocus);
        }

        private void Client_SizeChanged(object sender, EventArgs e)
        {
        }

        private void newBotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.Accounts == null)
            {
                MessageBox.Show("You must register an account in Account Manager first!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                AccountManager am = new AccountManager();
                am.MdiParent = this;
                am.Show();
                return;
            }
            else if (Properties.Settings.Default.JavaDirectory == "null")
            {
                MessageBox.Show("Please set the Java JRE Directory up in Settings! Hint: Use the Find JRE button.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Settings s = new Settings();
                s.MdiParent = this;
                s.Show();
                return;
            }
            NewBot b = new NewBot(this);
            b.MdiParent = this;
            b.Show();
        }

        private void textBox1_GotFocus(object sender, EventArgs e)
        {
            Form frm = this.ActiveMdiChild;
            if (frm is Bot)
            {
                Bot b = (Bot)frm;
                b.focused = true;
            }
        }

        private void textBox1_LostFocus(object sender, EventArgs e)
        {
            Form frm = this.ActiveMdiChild;
            if (frm is Bot)
            {
                Bot b = (Bot)frm;
                b.focused = false;
            }
        }

        public void focusClicked(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            foreach (Form frm in this.MdiChildren)
            {
                if (item.Text == frm.Text)
                {
                    frm.Focus();
                    return;
                }
            }
        }

        private void accountManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccountManager am = new AccountManager();
            am.MdiParent = this;
            am.Show();
        }

        private void settingsaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings s = new Settings();
            s.MdiParent = this;
            s.Show();
        }

        private bool checkConsistency()
        {
            foreach (Form frm in this.MdiChildren)
            {
                if (!(frm.Text.IndexOf("Bot ") > -1))
                {
                    return false;
                }
            }
            return true;
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!checkConsistency())
            {
                return;
            }
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void horizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!checkConsistency())
            {
                return;
            }
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void verticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!checkConsistency())
            {
                return;
            }
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void humanInputToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (humanInputToolStripMenuItem.Checked)
                humanInputToolStripMenuItem.Checked = false;
            else
                humanInputToolStripMenuItem.Checked = true;
        }

        private void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = this.ActiveMdiChild;
            if (frm is Bot)
            {
                Bot b = (Bot)frm;
                if (b.stub.botScript == null)
                {
                    ScriptManager SM = new ScriptManager(b);
                    SM.MdiParent = this;
                    SM.Show();
                    return;
                }
                if (b.stub.botScript.scriptRunning && (!b.stub.botScript.scriptPaused))
                    return;
                else if (b.stub.botScript.scriptPaused)
                    b.stub.botScript.Start();
                else if (!b.stub.botScript.scriptRunning)
                {
                    ScriptManager SM = new ScriptManager(b);
                    SM.MdiParent = this;
                    SM.Show();
                }
            }
        }

        private void pauseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = this.ActiveMdiChild;
            if (frm is Bot)
            {
                Bot b = (Bot)frm;
                if (b.stub.botScript != null)
                {
                    if (b.stub.botScript.scriptRunning && (!b.stub.botScript.scriptPaused))
                    {
                        b.stub.botScript.Pause();
                        return;
                    }
                }
            }
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = this.ActiveMdiChild;
            if (frm is Bot)
            {
                Bot b = (Bot)frm;
                if (b.stub.botScript != null)
                {
                    if (b.stub.botScript.scriptRunning)
                    {
                        b.stub.botScript.Stop();
                        return;
                    }
                }
            }
        }

        private void terminateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = this.ActiveMdiChild;
            if (frm is Bot)
            {
                Bot b = (Bot)frm;
                if ((b.stub.botScript != null))
                {
                    b.stub.botScript.Terminate();
                    return;
                }
            }
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Copy();
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.SelectAll();
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = this.ActiveMdiChild;
            if (frm is Bot)
            {
                Bot b = (Bot)frm;
                b.stub.botLog.Clear();
            }
        }

        private void developerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = this.ActiveMdiChild;
            if (frm is Bot)
            {
                Bot b = (Bot)frm;
                Developer d = new Developer(b.stub.botCanvas.GetCanvas(0, false));
                d.Show();
            }
        }

        public Scion.Library.RS.Tile tile;

        private void setCoordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = this.ActiveMdiChild;
            if (frm is Bot)
            {
                Bot b = (Bot)frm;
                tile = b.stub.botReflection.GetCharacterTile();
            }
        }

        private void calculateDistanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = this.ActiveMdiChild;
            if (frm is Bot)
            {
                Bot b = (Bot)frm;
                if (tile != null)
                {
                    Superior s = new Superior();
                    s.baseStub = b.stub;
                    s.Log.WriteLine(s.DistanceToTile(tile).ToString());
                }
            }
        }

        private void scionBotWebsiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.scionbot.com/");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Jaco (Main Developer)\nNader (Reflection)\nDarkXeD (Reflection)\nFlaming Idiot (Tab Strip)\nJusten (Project Management)\n\n© 2009 ScionBot.com", "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
