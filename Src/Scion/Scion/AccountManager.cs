﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Scion.Misc;
using Scion.Properties;

namespace Scion
{
    /// <summary>
    /// The Account Manager form.
    /// </summary>
    public partial class AccountManager : Form
    {
        public AccountManager()
        {
            InitializeComponent();
        }

        private void AccountManager_Load(object sender, EventArgs e)
        {
            this.FormClosing += new FormClosingEventHandler(AccountManager_FormClosing);
            if (Scion.Properties.Settings.Default.Accounts == null)
            {
                Scion.Properties.Settings.Default.Accounts = new System.Collections.ArrayList();
                Scion.Properties.Settings.Default.Save();
            }
            LoadAccounts();
        }

        private void AccountManager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Scion.Properties.Settings.Default.Accounts.Count == 0)
            {
                Scion.Properties.Settings.Default.Accounts = null;
                Scion.Properties.Settings.Default.Save();
            }
        }

        private void LoadAccounts()
        {
            listBox1.Items.Clear();
            foreach (string s in Scion.Properties.Settings.Default.Accounts)
            {
                try
                {
                    string decrypt = AccountCrypt.DecryptUsername(s);
                    listBox1.Items.Add(decrypt);
                }
                catch
                {
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string username = textBox1.Text;
            string password = textBox2.Text;
            if ((username == "") || (password == ""))
            {
                MessageBox.Show("Username and/or password is invalid.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string combine = username.Trim() + ":" + password.Trim();
            string encrypted = AccountCrypt.Encrypt(combine);
            Scion.Properties.Settings.Default.Accounts.Add(encrypted);
            textBox1.Clear();
            textBox2.Clear();
            LoadAccounts();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadAccounts();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to clear all the accounts?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == DialogResult.No)
                return;
            Scion.Properties.Settings.Default.Accounts = new System.Collections.ArrayList();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to delete the selected account?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == DialogResult.No)
                return;
            if (listBox1.SelectedIndex < 0)
            {
                return;
            }
            Scion.Properties.Settings.Default.Accounts.RemoveAt(listBox1.SelectedIndex);
            LoadAccounts();
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            Scion.Properties.Settings.Default.Save();
            this.Close();
        }
    }
}
