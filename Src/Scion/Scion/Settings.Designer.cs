﻿namespace Scion
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.CNetHelpProvider = new System.Windows.Forms.HelpProvider();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.CNetHelpProvider.SetHelpKeyword(this.tabPage1, "Scion_Form_Settings.htm#Settings_tabPage1_0");
            this.CNetHelpProvider.SetHelpNavigator(this.tabPage1, System.Windows.Forms.HelpNavigator.Topic);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.CNetHelpProvider.SetShowHelp(this.tabPage1, true);
            this.tabPage1.Size = new System.Drawing.Size(262, 159);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Bot";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.CNetHelpProvider.SetHelpKeyword(this.groupBox1, "Scion_Form_Settings.htm#Settings_groupBox1");
            this.CNetHelpProvider.SetHelpNavigator(this.groupBox1, System.Windows.Forms.HelpNavigator.Topic);
            this.groupBox1.Location = new System.Drawing.Point(6, 0);
            this.groupBox1.Name = "groupBox1";
            this.CNetHelpProvider.SetShowHelp(this.groupBox1, true);
            this.groupBox1.Size = new System.Drawing.Size(250, 156);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.CNetHelpProvider.SetHelpKeyword(this.label3, "Scion_Form_Settings.htm#Settings_label3");
            this.CNetHelpProvider.SetHelpNavigator(this.label3, System.Windows.Forms.HelpNavigator.Topic);
            this.label3.Location = new System.Drawing.Point(94, 68);
            this.label3.Name = "label3";
            this.CNetHelpProvider.SetShowHelp(this.label3, true);
            this.label3.Size = new System.Drawing.Size(154, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Ex: C:\\...\\Java\\jre1.6.0_04\\bin";
            // 
            // button3
            // 
            this.CNetHelpProvider.SetHelpKeyword(this.button3, "Scion_Form_Settings.htm#Settings_button3");
            this.CNetHelpProvider.SetHelpNavigator(this.button3, System.Windows.Forms.HelpNavigator.Topic);
            this.button3.Location = new System.Drawing.Point(169, 127);
            this.button3.Name = "button3";
            this.CNetHelpProvider.SetShowHelp(this.button3, true);
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Find JRE";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox2
            // 
            this.CNetHelpProvider.SetHelpKeyword(this.textBox2, "Scion_Form_Settings.htm#Settings_textBox2");
            this.CNetHelpProvider.SetHelpNavigator(this.textBox2, System.Windows.Forms.HelpNavigator.Topic);
            this.textBox2.Location = new System.Drawing.Point(97, 45);
            this.textBox2.Name = "textBox2";
            this.CNetHelpProvider.SetShowHelp(this.textBox2, true);
            this.textBox2.Size = new System.Drawing.Size(147, 20);
            this.textBox2.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.CNetHelpProvider.SetHelpKeyword(this.label2, "Scion_Form_Settings.htm#Settings_label2");
            this.CNetHelpProvider.SetHelpNavigator(this.label2, System.Windows.Forms.HelpNavigator.Topic);
            this.label2.Location = new System.Drawing.Point(6, 48);
            this.label2.Name = "label2";
            this.CNetHelpProvider.SetShowHelp(this.label2, true);
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Java Directory:";
            // 
            // textBox1
            // 
            this.CNetHelpProvider.SetHelpKeyword(this.textBox1, "Scion_Form_Settings.htm#Settings_textBox1");
            this.CNetHelpProvider.SetHelpNavigator(this.textBox1, System.Windows.Forms.HelpNavigator.Topic);
            this.textBox1.Location = new System.Drawing.Point(97, 19);
            this.textBox1.Name = "textBox1";
            this.CNetHelpProvider.SetShowHelp(this.textBox1, true);
            this.textBox1.Size = new System.Drawing.Size(147, 20);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.CNetHelpProvider.SetHelpKeyword(this.label1, "Scion_Form_Settings.htm#Settings_label1");
            this.CNetHelpProvider.SetHelpNavigator(this.label1, System.Windows.Forms.HelpNavigator.Topic);
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.CNetHelpProvider.SetShowHelp(this.label1, true);
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Refresh Interval:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.CNetHelpProvider.SetHelpKeyword(this.tabControl1, "Scion_Form_Settings.htm#Settings_tabControl1");
            this.CNetHelpProvider.SetHelpNavigator(this.tabControl1, System.Windows.Forms.HelpNavigator.Topic);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.CNetHelpProvider.SetShowHelp(this.tabControl1, true);
            this.tabControl1.Size = new System.Drawing.Size(270, 185);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(262, 159);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Override";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBox1);
            this.groupBox2.Location = new System.Drawing.Point(6, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(250, 153);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(15, 19);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(136, 17);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "Enable Aero on Startup";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.CNetHelpProvider.SetHelpKeyword(this.button1, "Scion_Form_Settings.htm#Settings_button1");
            this.CNetHelpProvider.SetHelpNavigator(this.button1, System.Windows.Forms.HelpNavigator.Topic);
            this.button1.Location = new System.Drawing.Point(207, 203);
            this.button1.Name = "button1";
            this.CNetHelpProvider.SetShowHelp(this.button1, true);
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Apply";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.CNetHelpProvider.SetHelpKeyword(this.button2, "Scion_Form_Settings.htm#Settings_button2");
            this.CNetHelpProvider.SetHelpNavigator(this.button2, System.Windows.Forms.HelpNavigator.Topic);
            this.button2.Location = new System.Drawing.Point(126, 203);
            this.button2.Name = "button2";
            this.CNetHelpProvider.SetShowHelp(this.button2, true);
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // CNetHelpProvider
            // 
            this.CNetHelpProvider.HelpNamespace = "Scion.chm";
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(292, 237);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tabControl1);
            this.CNetHelpProvider.SetHelpKeyword(this, "Scion_Form_Settings.htm");
            this.CNetHelpProvider.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Settings";
            this.CNetHelpProvider.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.HelpProvider CNetHelpProvider;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBox1;

    }
}