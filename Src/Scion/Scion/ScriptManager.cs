﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Scion.Misc;
using System.IO;
using Scion.Core;

namespace Scion
{
    /// <summary>
    /// The Script Manager form.
    /// </summary>
    public partial class ScriptManager : Form
    {
        Bot bot;
        List<FileInfo> scripts;

        public ScriptManager(Bot b)
        {
            bot = b;
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                bot.stub.ExecuteScript(listBox1.Text);
                this.Close();
            }
        }

        private void ScriptManager_Load(object sender, EventArgs e)
        {
            scripts = ScionScriptManager.GetScripts();
            if (scripts != null)
            {
                foreach (FileInfo file in scripts)
                {
                    string name = file.Name;
                    name = name.Remove(name.LastIndexOf("."));
                    listBox1.Items.Add(name);
                }
            }
            textBox1.BackColor = SystemColors.Window;
            textBox2.BackColor = SystemColors.Window;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                textBox1.Text = Script.GetScriptAuthor(listBox1.Text);
                textBox2.Text = Script.GetScriptComment(listBox1.Text);
            }
        }
    }
}
