﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scion.Core.Updater
{
    public class Fields
    {
        public static string BaseX = "ld.V";
        public static string BaseY = "kj.t";

        public static string MyPlayer = "il.d";
        public static string QueueX = "qa.fc";
        public static string QueueY = "qa.Jb";
        public static string CharX = "qa.U"; //implement into updater
        public static string CharY = "qa.lb"; //implement into updater

        public static string MiniMapScale = "oi.n";
        public static string CompassAngle = "lk.b";
        public static string MapOffsetAngle = "mh.v";
        public static string CameraPositionZ = "qk.d";

        public static string Animation = "qa.ub"; //implement into updater
        public static string LoopCycle = "nd.r"; //implement into updater
        public static string LoopStatus = "qa.y"; //implement into updater
        public static string Motion = "qa.Lb"; //implement into updater
    }
}
