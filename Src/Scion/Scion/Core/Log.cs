﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Scion.Core
{
    /// <summary>
    /// This class performs basic I/O operations to write the bot's log.
    /// </summary>
    public class Log
    {
        private delegate void UpdateLogDelegate(string text);

        Stub stub;
        /// <summary>
        /// The stream of the Log.
        /// </summary>
        public MemoryStream logStream;
        /// <summary>
        /// The Log Reader.
        /// </summary>
        public StreamReader logStreamReader;
        /// <summary>
        /// The Log Writer.
        /// </summary>
        public StreamWriter logStreamWriter;

        public Log(Stub s)
        {
            stub = s;
            logStream = new MemoryStream();
            logStreamReader = new StreamReader(logStream);
            logStreamWriter = new StreamWriter(logStream);
        }

        private void UpdateLog(string text)
        {
            stub.bot.client.textBox1.Invoke(new UpdateLogDelegate(stub.bot.UpdateLog), new object[] {text});
        }

        /// <summary>
        /// Flush the Log stream.
        /// </summary>
        public void Flush()
        {
            try
            {
                logStreamWriter.Flush();
                UpdateLog(DumpStream());
            }
            catch
            {
            }
        }

        /// <summary>
        /// Writes a line to the Log stream.
        /// </summary>
        /// <param name="str">The line to write.</param>
        /// <returns>Returns true if the function succeeds.</returns>
        public bool WriteLine(string str)
        {
            try
            {
                string minute;
                string second;
                if (DateTime.Now.Minute < 10)
                    minute = "0" + DateTime.Now.Minute;
                else
                    minute = DateTime.Now.Minute.ToString();
                if (DateTime.Now.Second < 10)
                    second = "0" + DateTime.Now.Second;
                else
                    second = DateTime.Now.Second.ToString();
                logStreamWriter.WriteLine("[" + DateTime.Now.Hour % 12 + ":" + minute + ":" + second + "] " + str);
                Flush();
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Reads a line from the Log stream.
        /// </summary>
        /// <returns>Returns the string that was read.</returns>
        public string ReadLine()
        {
            try
            {
                string str = logStreamReader.ReadLine();
                return str;
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Clears the log.
        /// </summary>
        public void Clear()
        {
            logStream = new MemoryStream();
            logStreamReader = new StreamReader(logStream);
            logStreamWriter = new StreamWriter(logStream);
            Flush();
        }

        /// <summary>
        /// Dumps the entire Log stream into a string.
        /// </summary>
        /// <returns>Returns the Log stream dump.</returns>
        public string DumpStream()
        {
            string str = "";
            logStream.Seek(0, SeekOrigin.Begin);
            while (!logStreamReader.EndOfStream)
            {
                try
                {
                    str += logStreamReader.ReadLine() + System.Environment.NewLine;
                }
                catch
                {
                }
            }
            return str;
        }
    }
}
