﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scion.Library.Antiban;

namespace Scion.Core
{
    /// <summary>
    /// The Keyboard property is used for keyboard input onto the RS Client.
    /// </summary>
    public class Keyboard
    {
        Stub stub;

        public Keyboard(Stub s)
        {
            stub = s;
        }

        /// <summary>
        /// Press a single key.
        /// </summary>
        /// <param name="character">Character of the key.</param>
        /// <returns>Returns true if the functions succeeds.</returns>
        public bool PressKey(char character)
        {
            bool result = stub.botBridge.SendWait("PressKey " + character.ToString());
            return result;
        }

        /// <summary>
        /// Press enter.
        /// </summary>
        /// <param name="sleep">How long to wait before the enter key is released.</param>
        /// <returns>Returns true if the function succeeds.</returns>
        public bool PressEnter(SleepRandom sleep)
        {
            bool result = stub.botBridge.SendWait("PressEnter " + sleep.random);
            return result;
        }

        /// <summary>
        /// Presses backspace.
        /// </summary>
        /// <returns>Returns true if the function succeeds.</returns>
        public bool PressBackSpace(SleepRandom sleep)
        {
            bool result = stub.botBridge.SendWait("PressBackSpace " + sleep.random);
            return result;
        }

        /// <summary>
        /// Press spacebar.
        /// </summary>
        /// <returns>Returns true if the function succeeds.</returns>
        public bool PressSpace()
        {
            bool result = stub.botBridge.SendWait("PressSpace");
            return result;
        }

        /// <summary>
        /// Presses an arrow.
        /// </summary>
        /// <param name="key">The key to press. Up = 1, Down = 2, Left = 3, Right = 4.</param>
        /// <param name="time">How long to wait before the arrow is key is released.</param>
        /// <returns>Returns true if the function succeeds.</returns>
        public bool PressArrow(int key, SleepRandom time)
        {
            bool result = stub.botBridge.SendWait("PressArrow " + key.ToString() + " " + time.random.ToString()); ;
            return result;
        }

        /// <summary>
        /// Holds the specified arrow key.
        /// </summary>
        /// <param name="key">The key to hold.</param>
        /// <returns>Returns true if the function succeeds.</returns>
        public bool HoldArrow(int key)
        {
            return stub.botBridge.SendWait("HoldArrow " + key);
        }

        /// <summary>
        /// Releases the specified arrow key.
        /// </summary>
        /// <param name="key">The key to release.</param>
        /// <returns>Returns true if the function succeeds.</returns>
        public bool ReleaseArrow(int key)
        {
            return stub.botBridge.SendWait("ReleaseArrow " + key);
        }

        /// <summary>
        /// Types a string of text.
        /// </summary>
        /// <param name="message">The message to type.</param>
        /// <param name="interval">The interval of how long to wait before the next key press.</param>
        /// <returns></returns>
        public bool Type(string message, TypeRandom interval)
        {
            if (interval.maximumValue >= interval.intervalValue)
                return false;
            foreach (char c in message)
            {
                if ((int)c != 32)
                    PressKey(c);
                else
                    PressSpace();
                System.Threading.Thread.Sleep(interval.random);
                interval = new TypeRandom(interval.intervalValue, interval.maximumValue);
            }
            return true;
        }
    }
}
