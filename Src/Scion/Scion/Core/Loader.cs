﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Net.Sockets;

namespace Scion.Core
{
    /// <summary>
    /// This class executes the JVM with the Java botloader class. Standard I/O are then redirected a stream
    /// which are thrown back to the stub.
    /// </summary>
    public class Loader
    {
        public Process botProcess = null;
        public StreamWriter botBasicInput = null;
        public StreamReader botBasicOutput = null;
        public StreamReader botOutput = null;
        public StreamWriter botInput = null;
        public TcpClient botSocket = null;

        Stub super;

        public Loader(Stub parent)
        {
            super = parent;
        }

        private int DetermineBool(bool trueFalse)
        {
            if (trueFalse)
                return 1;
            else
                return 0;
        }

        //Use the Process class in order to load the bot. Redirect the Standard I/O to two streams located
        //in Stub.
        public void Load()
        {
            Process p = new Process();
            p.StartInfo.WorkingDirectory = System.Windows.Forms.Application.StartupPath;
            //We're executing the JVM here.
            p.StartInfo.FileName = Misc.ScionSettings.JavaDirectory + @"\java.exe";
            //Tell the JVM to run Bot.class with these arguments. Also, tell the JVM to add the runescape.jar
            //into the stack.
            p.StartInfo.Arguments = @"-cp .;loader; Bot " + super.botNumber + " " + super.botAccount.world + " " + DetermineBool(super.botAccount.members) + " " + super.port;
            //This is used to redirect the I/O.
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardInput = true;
            //p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.StartInfo.CreateNoWindow = true;
            //Start the bot.
            p.Start();
            //Push the Standard I/O streams to the botOutput & botInput objects in Stub.cs.
            botProcess = p;
            botBasicOutput = botProcess.StandardOutput;
            botBasicInput = botProcess.StandardInput;
            //Run a loop until the Java-end tells us that the bot's GUI fully intialized.
            while (true)
            {
                string message = botBasicOutput.ReadLine();
                if (message.IndexOf("Initialized") > -1)
                    break;
                //Makes the CPU very low consumption.
                Thread.Sleep(100);
            }
            while (true)
            {
                string message = botBasicOutput.ReadLine();
                if (message.IndexOf("WaitingHandshake") > -1)
                    break;
                Thread.Sleep(100);
            }
            botSocket = new TcpClient("localhost", super.port);
            botInput = new StreamWriter(botSocket.GetStream());
            botOutput = new StreamReader(botSocket.GetStream());
            while (true)
            {
                string message = botBasicOutput.ReadLine();
                if (message.IndexOf("HandshakeAccepted") > -1)
                    break;
                Thread.Sleep(100);
            }
        }
    }
}
