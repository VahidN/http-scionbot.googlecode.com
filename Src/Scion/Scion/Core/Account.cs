﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scion.Library.Underlying;
using System.Drawing;
using Scion.Library;
using System.Threading;

namespace Scion.Core
{
    /// <summary>
    /// Class that hold the basic data for an account.
    /// </summary>
    public class Account
    {
        Stub stub;
        /// <summary>
        /// Username of account.
        /// </summary>
        public string username;
        /// <summary>
        /// Password of account.
        /// </summary>
        public string password;
        /// <summary>
        /// World the account is in.
        /// </summary>
        public int world;
        /// <summary>
        /// Account is members or not.
        /// </summary>
        public bool members;
        /// <summary>
        /// Disable auto login.
        /// </summary>
        public bool DisableAutoLogin = false;

        public Account(string user, string pass, int wrld, bool mem)
        {
            username = user;
            password = pass;
            world = wrld;
            members = mem;
        }

        public void PushStub(Stub s)
        {
            stub = s;
        }

        public bool AccountLogin()
        {
            return stub.botRandom.ProcessRandom("Login");
        }
    }
}
