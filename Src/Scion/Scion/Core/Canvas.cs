﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scion.Core;
using System.Drawing;
using Scion.Library.Underlying;
using System.Threading;
using Scion.Library;

namespace Scion.Core
{
    /// <summary>
    /// Gets the RS Client's image.
    /// </summary>
    public class Canvas : Determine
    {
        Stub stub;

        public Canvas(Stub s)
        {
            stub = s;
        }

        /// <summary>
        /// Gets the RS Client's image. This method filters the image and drops all failed captured canvases.
        /// </summary>
        /// <param name="efficiency">If the canvas fails to capture, this is how long (MS) the bot will take
        /// before trying again. Set to 0 to avoid this feature.</param>
        /// <returns>The canvas image.</returns>
        public Bitmap GetCanvas(int efficiency)
        {
            Retry:
            Bitmap bmp = stub.botOffscreen.CaptureRawWindow();
            int counter = 0;
            for (int I = 0; I < 5; I++)
            {
                int X = I * 150;
                int Y = 471;
                if (IsTolerance(bmp.GetPixel(X, Y), Color.FromArgb(255,255,255), 2))
                    counter++;
            }
            if (counter == 0)
                return bmp;
            else
            {
                bmp.Dispose();
                if (efficiency != 0)
                    Thread.Sleep(efficiency);
                goto Retry;
            }
        }

        /// <summary>
        /// Gets the RS Client's image. This method filters the image and drops all failed captured canvases.
        /// </summary>
        /// <param name="efficiency">If the canvas fails to capture, this is how long (MS) the bot will take
        /// before trying again. Set to 0 to avoid this feature.</param>
        /// <param name="dispose">Dispose the bitmap the buffer was create from.</param>
        /// <returns>The buffered image of the canvas.</returns>
        public BufferedImage GetCanvas(int efficiency, bool dispose)
        {
            Bitmap bmp = GetCanvas(efficiency);
            BufferedImage bi = new BufferedImage(bmp);
            if (dispose)
                bmp.Dispose();
            return bi;
        }
    }
}
