﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Scion.Library;
using System.Threading;

namespace Scion.Core
{
    /// <summary>
    /// An interface between the bot form and PaintObject. Basically the script will push the PaintObject
    /// to the script, and the bot form will read from the object list and include the paint object within the
    /// next painting.
    /// </summary>
    public class GFX
    {
        /// <summary>
        /// The list of object collectd by AddObject.
        /// </summary>
        public List<PaintObject> objects = new List<PaintObject>();
        
        Stub stub;

        public GFX(Stub s)
        {
            stub = s;
        }

        /// <summary>
        /// Find the PaintObject in the object list.
        /// </summary>
        /// <param name="pObject">The PaintObject to look for.</param>
        /// <returns>Returns true if the object specified is found.</returns>
        public bool FindObject(PaintObject pObject)
        {
            foreach (PaintObject po in objects)
            {
                if (po.Equals(pObject))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the index of the PaintObject in the list.
        /// </summary>
        /// <param name="po">The PaintObject to look for.</param>
        /// <returns>Returns the index of the PaintObject in the list.</returns>
        public int GetObjectIndex(PaintObject po)
        {
            for (int I = 0; I < objects.Count; I++)
            {
                if (objects[I].Equals(po))
                    return I;
            }
            return -1;
        }

        /// <summary>
        /// Add a PaintObject to the list.
        /// </summary>
        /// <param name="po">The PaintObject to append.</param>
        public void AddObject(PaintObject po)
        {
            objects.Add(po);
        }

        /// <summary>
        /// Removes a PaintObject from the list.
        /// </summary>
        /// <param name="po">The PaintObject to remove.</param>
        public void RemoveObject(PaintObject po)
        {
            for (int I = (objects.Count - 1); I > -1; I--)
            {
                if (objects[I].Equals(po))
                {
                    objects.RemoveAt(I);
                    break;
                }
            }
        }

        /// <summary>
        /// Remove all the objects in the list. Used to free all the PaintObjects created.
        /// </summary>
        /// <param name="removeCursor">Remove the cursor (true), leave the cursor (false).</param>
        public void RemoveAllObjects(bool removeCursor)
        {
            for (int I = (objects.Count - 1); I > -1; I--)
            {
                if (!objects[I].Equals(stub.mouse.botCursor))
                    objects.RemoveAt(I);
                else
                {
                    if (removeCursor)
                        objects.RemoveAt(I);
                }
            }
        }
    }
}
