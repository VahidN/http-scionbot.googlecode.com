﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Scion.Library.Antiban;
using Scion.Library;

namespace Scion.Core
{
    /// <summary>
    /// The Mouse property is used for mouse input onto the RS Client.
    /// </summary>
    public class Mouse
    {
        /// <summary>
        /// The botCursor that is painted to the canvas.
        /// </summary>
        public PaintObject botCursor;

        Stub stub;
        int currentX = -1;
        int currentY = -1;

        public Mouse(Stub s)
        {
            stub = s;
            botCursor = new PaintObject(Pens.Red, new Rectangle(376, 245, 6, 6), true);
            stub.botGraphics.AddObject(botCursor);
        }

        /// <summary>
        /// Gets/Sets the Current X-Axis position the mouse is in.
        /// </summary>
        public int CurrentX
        {
            get
            {
                if (currentX == -1)
                {
                    string result = stub.botBridge.SendInformation("GetCurrentX");
                    try
                    {
                        int value = int.Parse(result);
                        return value;
                    }
                    catch
                    {
                        return -1;
                    }
                }
                else
                {
                    return currentX;
                }
            }
            set
            {
                currentX = value;
                stub.botBridge.SendWait("CurrentX " + value.ToString());
            }
        }

        /// <summary>
        /// Gets/Sets the Current Y-Axis position the mouse is in.
        /// </summary>
        public int CurrentY
        {
            get
            {
                if (currentY == -1)
                {
                    string result = stub.botBridge.SendInformation("GetCurrentY");
                    try
                    {
                        int value = int.Parse(result);
                        return value;
                    }
                    catch
                    {
                        return -1;
                    }
                }
                else
                {
                    return currentY;
                }
            }
            set
            {
                currentY = value;
                stub.botBridge.SendWait("CurrentY " + value.ToString());
            }
        }

        /// <summary>
        /// Gets/Sets the current speed the mouse is traveling at when moving.
        /// </summary>
        public int Speed
        {
            get
            {
                string result = stub.botBridge.SendInformation("GetSpeed");
                try
                {
                    int value = int.Parse(result);
                    return value;
                }
                catch
                {
                    return -1;
                }
            }
            set
            {
                stub.botBridge.SendWait("Speed " + value.ToString());
            }
        }

        /// <summary>
        /// Gets/Sets the reaction time before the mouse is about to move.
        /// </summary>
        public int ReactionTime
        {
            get
            {
                string result = stub.botBridge.SendInformation("GetReaction");
                try
                {
                    int value = int.Parse(result);
                    return value;
                }
                catch
                {
                    return -1;
                }
            }
            set
            {
                stub.botBridge.SendWait("Reaction " + value.ToString());
            }
        }

        /// <summary>
        /// Gets/Sets the MS time it takes to move each bit.
        /// </summary>
        public int MSPerBit
        {
            get
            {
                string result = stub.botBridge.SendInformation("GetPerBit");
                try
                {
                    int value = int.Parse(result);
                    return value;
                }
                catch
                {
                    return -1;
                }
            }
            set
            {
                stub.botBridge.SendWait("PerBit " + value.ToString());
            }
        }

        /// <summary>
        /// Moves the mouse to the specified location and then clicks at that specific location.
        /// </summary>
        /// <param name="point">The random point at which to click/move at.</param>
        /// <param name="leftRight">Click left button (true), or right button (false).</param>
        /// <returns>Returns true if the functions succeeds.</returns>
        public bool MoveClick(Point point, bool leftRight)
        {
            currentX = point.X;
            currentY = point.Y;
            bool result = stub.botBridge.SendWait("Move " + currentX + " " + currentY + " 0 0");
            System.Threading.Thread.Sleep(new SleepRandom(10, 20).random);
            if (result)
            {
                Point po = new Point(CurrentX, CurrentY);
                result = stub.botBridge.SendWait("Click " + po.X.ToString() + " " + po.Y.ToString() + " " + leftRight.ToString());
                botCursor.rect = new Rectangle(po.X, po.Y, botCursor.rect.Width, botCursor.rect.Height);
            }
            return result;
        }

        /// <summary>
        /// Moves the mouse to the specified location and then clicks at that specific location.
        /// </summary>
        /// <param name="point">The random point at which to click/move at.</param>
        /// <param name="leftRight">Click left button (true), or right button (false).</param>
        /// <returns>Returns true if the functions succeeds.</returns>
        public bool MoveClick(PointRandom point, bool leftRight)
        {
            currentX = point.random.X;
            currentY = point.random.Y;
            int rndX = point.random.X - point.pointXValue;
            int rndY = point.random.Y - point.pointYValue;
            bool result = stub.botBridge.SendWait("Move " + point.pointXValue.ToString() + " " + point.pointYValue.ToString() + " " + rndX.ToString() + " " + rndY.ToString());
            System.Threading.Thread.Sleep(new SleepRandom(10, 20).random);
            if (result)
            {
                Point po = new Point(CurrentX, CurrentY);
                result = stub.botBridge.SendWait("Click " + po.X.ToString() + " " + po.Y.ToString() + " " + leftRight.ToString());
                botCursor.rect = new Rectangle(po.X, po.Y, botCursor.rect.Width, botCursor.rect.Height);
            }
            return result;
        }

        /// <summary>
        /// Move the mouse to a specified location.
        /// </summary>
        /// <param name="point">The point to move to.</param>
        /// <returns>Returns true of the functions succeeds.</returns>
        public bool MoveMouse(PointRandom point)
        {
            currentX = point.random.X;
            currentY = point.random.Y;
            int rndX = point.random.X - point.pointXValue;
            int rndY = point.random.Y - point.pointYValue;
            bool result = stub.botBridge.SendWait("Move " + point.pointXValue.ToString() + " " + point.pointYValue.ToString() + " " + rndX.ToString() + " " + rndY.ToString());
            botCursor.rect = new Rectangle(point.random.X, point.random.Y, botCursor.rect.Width, botCursor.rect.Height);
            return result;
        }

        /// <summary>
        /// Move the mouse to a specified location.
        /// </summary>
        /// <param name="point">The point to move to.</param>
        /// <returns>Returns true of the functions succeeds.</returns>
        public bool MoveMouse(Point point)
        {
            currentX = point.X;
            currentY = point.Y;
            bool result = stub.botBridge.SendWait("Move " + currentX + " " + currentY + " 0 0");
            botCursor.rect = new Rectangle(currentX, currentY, botCursor.rect.Width, botCursor.rect.Height);
            return result;
        }

        /// <summary>
        /// Click the mouse at a specific location.
        /// </summary>
        /// <param name="point">The point at which to click at.</param>
        /// <param name="leftRight">Click left button (true), or right button (false).</param>
        /// <returns>Returns true if the function succeeds.</returns>
        public bool ClickMouse(PointRandom point,  bool leftRight)
        {
            currentX = point.random.X;
            currentY = point.random.Y;
            botCursor.rect = new Rectangle(point.random.X, point.random.Y, botCursor.rect.Width, botCursor.rect.Height);
            bool result = stub.botBridge.SendWait("Click " + point.random.X.ToString() + " " + point.random.Y.ToString() + " " + leftRight.ToString());
            return result;
        }

        /// <summary>
        /// Keep the mouse down at the specified location.
        /// </summary>
        /// <param name="pos">The position to keep it down at.</param>
        /// <param name="leftRight">Down on left button (true), or right button (false).</param>
        /// <returns>Returns true if the function succeeds.</returns>
        public bool MouseDown(Point pos, bool leftRight)
        {
            currentX = pos.X;
            currentY = pos.Y;
            bool result = stub.botBridge.SendWait("Down " + pos.X.ToString() + " " + pos.Y.ToString() + " " + leftRight.ToString());
            return result;
        }

        /// <summary>
        /// Keep the mouse down at the current location.
        /// </summary>
        /// <param name="leftRight">Dowe on left button (true), or right button (false).</param>
        /// <returns>Returns true if the function succeeds.</returns>
        public bool MouseDown(bool leftRight)
        {
            bool result = stub.botBridge.SendWait("Down -1 -1 " + leftRight.ToString());
            return result;
        }

        /// <summary>
        /// Releases the mouse at the specified location.
        /// </summary>
        /// <param name="pos">The position to release the mouse.</param>
        /// <param name="leftRight">Up on left button (true), or right button (false).</param>
        /// <returns>Returns true if the function succeeds.</returns>
        public bool MouseUp(Point pos, bool leftRight)
        {
            currentX = pos.X;
            currentY = pos.Y;
            bool result = stub.botBridge.SendWait("Up " + pos.X.ToString() + " " + pos.Y.ToString() + " " + leftRight.ToString());
            return result;
        }

        /// <summary>
        /// Releases the mouse at the current location.
        /// </summary>
        /// <param name="leftRight">Up on left button (true), or right button (false).</param>
        /// <returns>Returns true if the function succeeds.</returns>
        public bool MouseUp(bool leftRight)
        {
            bool result = stub.botBridge.SendWait("Down -1 -1 " + leftRight.ToString());
            return result;
        }
    }
}
