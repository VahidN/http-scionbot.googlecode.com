﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;

namespace Scion.Core
{
    /// <summary>
    /// The bridge between Java and C#.
    /// </summary>
    public class Bridge
    {
        Stub stub;

        public Bridge(Stub s)
        {
            stub = s;
        }

        public void SyncronizeBridge()
        {
            WriteLine("GetSynchronize");
            while (true)
            {
                try
                {
                    string pushBack = GetInformation();
                    if (pushBack.IndexOf("Synchronize") > -1)
                        return;
                }
                catch
                {
                    return;
                }
            }
        }

        private bool DetermineResult(string data)
        {
            if (data.IndexOf("true") > -1)
                return true;
            else
                return false;
        }

        public string SendInformation(string data)
        {
            //Console.WriteLine(data);
            stub.botLoader.botInput.WriteLine(data);
            stub.botLoader.botInput.Flush();
            string result = stub.botLoader.botOutput.ReadLine();
            return result;
        }

        public bool SendWait(string data)
        {
            //Console.WriteLine(data);
            stub.botLoader.botInput.WriteLine(data);
            stub.botLoader.botInput.Flush();
            string str = stub.botLoader.botOutput.ReadLine();
            //Console.WriteLine(str);
            bool result = DetermineResult(str);
            return result;
        }

        public string GetInformation()
        {
            try
            {
                string str = stub.botLoader.botOutput.ReadLine();
                //Console.WriteLine(str);
                return str;
            }
            catch
            {
                return null;
            }
        }

        public void WriteLine(string data)
        {
            try
            {
                //Console.WriteLine(data);
                stub.botLoader.botInput.WriteLine(data);
                stub.botLoader.botInput.Flush();
            }
            catch
            {
            }
        }
    }
}
