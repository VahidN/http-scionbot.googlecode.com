﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Scion.Library.Antiban;

namespace Scion.Library.Underlying
{
    /// <summary>
    /// Class that determines if two objects/numbers are nearby each other.
    /// </summary>
    public class Determine
    {
        /// <summary>
        /// Checks to see if the numbers are within the range of each other with a tolerance.
        /// </summary>
        /// <param name="first">The number to check.</param>
        /// <param name="second">The number to check against.</param>
        /// <param name="tolerance">The tolerance to use.</param>
        /// <returns>Returns true if they are within bounds.</returns>
        public static bool IsWithinBounds(int first, int second, int tolerance)
        {
            if ((first > (second - tolerance - 1)) && (first < (second + tolerance + 1)))
                return true;
            return false;
        }

        /// <summary>
        /// Checks to see if the two colours match.
        /// </summary>
        /// <param name="c">First colour.</param>
        /// <param name="c2">Second colour.</param>
        /// <returns>Returns true if they match.</returns>
        public static bool IsMatch(Color c, Color c2)
        {
            if ((c.R == c2.R) && (c.G == c2.G) && (c.B == c2.B))
                return true;
            return false;
        }

        /// <summary>
        /// Checks to see if the two colours match.
        /// </summary>
        /// <param name="r">First colour R-Channel.</param>
        /// <param name="g">First colour G-Channel.</param>
        /// <param name="b">First colour B-Channel.</param>
        /// <param name="r2">Second colour R-Channel.</param>
        /// <param name="g2">Second colour G-Channel.</param>
        /// <param name="b2">Second colour B-Channel.</param>
        /// <returns>Returns true if they match.</returns>
        public static bool IsMatch(int r, int g, int b, int r2, int g2, int b2)
        {
            if ((r == r2) && (r == g2) && (r == b2))
                return true;
            return false;
        }

        /// <summary>
        /// Check to see if two colours are within the bounds of each other relative to the tolerance.
        /// </summary>
        /// <param name="c">First colour.</param>
        /// <param name="c2">Second colour.</param>
        /// <param name="tolerance">The tolerance to use.</param>
        /// <returns>Returns true if they match.</returns>
        public static bool IsTolerance(Color c, Color c2, int tolerance)
        {
            if ((c.R > (c2.R - tolerance - 1)) && (c.R < (c2.R + tolerance + 1)))
                if ((c.G > (c2.G - tolerance - 1)) && (c.G < (c2.G + tolerance + 1)))
                    if ((c.B > (c2.B - tolerance - 1)) && (c.B < (c2.B + tolerance + 1)))
                        return true;
            return false;
        }

        /// <summary>
        /// Check to see if two colours are within the bounds of each other relative to the tolerance.
        /// </summary>
        /// <param name="r">First colour R-Channel.</param>
        /// <param name="g">First colour G-Channel.</param>
        /// <param name="b">First colour B-Channel.</param>
        /// <param name="r2">Second colour R-Channel.</param>
        /// <param name="g2">Second colour G-Channel.</param>
        /// <param name="b2">Second colour B-Channel.</param>
        /// <param name="tolerance">The tolerance to use.</param>
        /// <returns>Returns true if they match.</returns>
        public static bool IsTolerance(int r, int g, int b, int r2, int g2, int b2, int tolerance)
        {
            if ((r > (r2 - tolerance - 1)) && (r < (r2 + tolerance + 1)))
                if ((g > (g2 - tolerance - 1)) && (g < (g2 + tolerance + 1)))
                    if ((b > (b2 - tolerance - 1)) && (b < (b2 + tolerance + 1)))
                        return true;
            return false;
        }

        /// <summary>
        /// Decides if the point found is valid or not.
        /// </summary>
        /// <param name="r">The point to determine.</param>
        /// <returns>Returns a boolean indicating if the point specified is valid or not.</returns>
        public bool Decide(PointRandom r)
        {
            if (r.pointXValue == -1 && r.pointYValue == -1)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Decides if the point found is valid or not.
        /// </summary>
        /// <param name="r">The point to determine.</param>
        /// <returns>Returns a boolean indicating if the point specified is valid or not.</returns>
        public bool Decide(Point r)
        {
            if (r.X == -1 && r.Y == -1)
                return false;
            else
                return true;
        }
    }
}
