﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Scion.Library.Underlying
{
    /// <summary>
    /// Class used for converting bitmaps into strings, vica-versa.
    /// </summary>
    public class BitmapString
    {
        /// <summary>
        /// Converts the image into a string.
        /// </summary>
        /// <param name="bmp">The bitmap to convert.</param>
        /// <param name="dispose">Dispose the bitmap the buffer was create from.</param>
        /// <returns>The string containing the bitmap.</returns>
        public static string Encrypt(BufferedImage bmp, bool dispose)
        {
            StringBuilder returnStr = new StringBuilder();
            returnStr.Append("(" + bmp.imageWidth.ToString() + "," + bmp.imageHeight.ToString() + ")|");
            for (int Y = 0; Y < bmp.imageHeight; Y++)
            {
                for (int X = 0; X < bmp.imageWidth; X++)
                {
                    Color c = bmp.GetPixel(X, Y);
                    returnStr.Append(c.R.ToString() + "," + c.G.ToString() + "," + c.B.ToString() + "|");
                }
            }
            if (dispose)
                bmp.image.Dispose();
            return returnStr.ToString();
        }

        /// <summary>
        /// Converts a string into a bitmap.
        /// </summary>
        /// <param name="str">The string of the image.</param>
        /// <returns>The bitmap generated from the string bitmap.</returns>
        public static Bitmap Decrypt(string str)
        {
            Bitmap bmp;
            string[] colourCluster = str.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            Color[] cluster = new Color[colourCluster.Length - 1];
            int counter = 0;
            int width = 0;
            int height = 0;
            foreach (string colour in colourCluster)
            {
                if (colour.IndexOf("(") > -1)
                {
                    string[] dimensions = colour.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    width = int.Parse(dimensions[0].Substring(1));
                    height = int.Parse(dimensions[1].Remove(dimensions[1].IndexOf(")")));
                }
                else if (colour.IndexOf("") > -1)
                {
                    string[] RGB = colour.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    int r = int.Parse(RGB[0]);
                    int g = int.Parse(RGB[1]);
                    int b = int.Parse(RGB[2]);
                    cluster[counter] = Color.FromArgb(r,g,b);
                    counter++;
                }
            }
            if (width == 0 || height == 0)
                return null;
            bmp = new Bitmap(width, height);
            counter = 0;
            for (int Y = 0; Y < height; Y++)
            {
                for (int X = 0; X < width; X++)
                {
                    bmp.SetPixel(X, Y, cluster[counter]);
                    counter++;
                }
            }
            return bmp;
        }

        /// <summary>
        /// Converts a string into a buffered image.
        /// </summary>
        /// <param name="str">The string of the image.</param>
        /// <param name="dispose">Dispose the bitmap the buffer was create from.</param>
        /// <returns>The buffered image generated from the string bitmap.</returns>
        public static BufferedImage Decrypt(string str, bool dispose)
        {
            Bitmap bmp = Decrypt(str);
            BufferedImage bi = new BufferedImage(bmp);
            if (dispose)
                bmp.Dispose();
            return bi;
        }
    }
}
