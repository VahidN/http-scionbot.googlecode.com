﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Scion.Library.Advanced;

namespace Scion.Library.Underlying
{
    /// <summary>
    /// Gathers every single relative pixel within the canvas in numerical order.
    /// </summary>
    public class Cluster : Determine
    {
        /// <summary>
        /// Counts all the pixels in the specified region.
        /// </summary>
        /// <param name="bmp">The bitmap to find the pixel in.</param>
        /// <param name="rect">The bounds to search in (X, Y, X2, Y2).</param>
        /// <param name="c">The colour to search for.</param>
        /// <param name="tolerance">The tolerance of the pixel.</param>
        /// <returns>Returns the number of similar pixels found in that region.</returns>
        public static int CountColours(BufferedImage bmp, Rectangle rect, Color c, int tolerance)
        {
            int returnCounter = 0;
            int r = c.R; int g = c.G; int b = c.B;
            for (int Y = rect.Y; Y < (rect.Height + 1); Y++)
            {
                for (int X = rect.X; X < (rect.Width + 1); X++)
                {
                    int tempR; int tempG; int tempB;
                    bmp.GetPixel(X, Y, out tempR, out tempG, out tempB);
                    if (IsTolerance(tempR, tempG, tempB, r, g, b, tolerance))
                        returnCounter++;
                }
            }
            return returnCounter;
        }

        /// <summary>
        /// Dumps a point cluster of every similar colour found in the specified region.
        /// </summary>
        /// <param name="bmp">The bitmap to find the pixel in.</param>
        /// <param name="rect">The bounds to search in (X, Y, X2, Y2).</param>
        /// <param name="c">The colour to search for.</param>
        /// <param name="tolerance">The tolerance of the pixel.</param>
        /// <returns>Returns the cluster of similar pixels found in that region.</returns>
        public static List<Point> DumpColours(BufferedImage bmp, Rectangle rect, Color c, int tolerance)
        {
            List<Point> points = new List<Point>();
            int r = c.R; int g = c.G; int b = c.B;
            for (int Y = rect.Y; Y < (rect.Height + 1); Y++)
            {
                for (int X = rect.X; X < (rect.Width + 1); X++)
                {
                    int tempR; int tempG; int tempB;
                    bmp.GetPixel(X, Y, out tempR, out tempG, out tempB);
                    if (IsTolerance(tempR, tempG, tempB, r, g, b, tolerance))
                        points.Add(new Point(X, Y));
                }
            }
            if (points.Count == 0)
                return null;
            return points;
        }


        /// <summary>
        /// Find a pixel with a tolerance.
        /// </summary>
        /// <param name="bmp">The bitmap to find the pixel in.</param>
        /// <param name="rect">The bounds to search in (X, Y, X2, Y2).</param>
        /// <param name="c">The colour to search for.</param>
        /// <param name="tolerance">The tolerance of the pixel.</param>
        /// <returns>Returns a list of points of all the colours found.</returns>
        public static List<Point> FindColour(BufferedImage bmp, Rectangle rect, Color c, int tolerance)
        {
            List<Point> points = new List<Point>();
            int r = c.R; int g = c.G; int b = c.B;
            for (int Y = rect.Y; Y < (rect.Height + 1); Y++)
            {
                for (int X = rect.X; X < (rect.Width + 1); X++)
                {
                    int tempR; int tempG; int tempB;
                    bmp.GetPixel(X, Y, out tempR, out tempG, out tempB);
                    if (IsTolerance(tempR, tempG, tempB, r, g, b, tolerance))
                        points.Add(new Point(X, Y));
                }
            }
            if (points.Count == 0)
                return null;
            else
                return points;
        }

        /// <summary>
        /// Find a colour in a spiral on the RS canvas.
        /// </summary>
        /// <param name="bmp">The bitmap to search in.</param>
        /// <param name="rect">The bounds to find the colour in.</param>
        /// <param name="start">The start location of the spiral.</param>
        /// <param name="c">The colour to find.</param>
        /// <param name="tolerance">The tolerance of the pixel to find.</param>
        /// <returns>Returns a list of points of all the colours found.</returns>
        public static List<Point> FindColourSpiral(BufferedImage bmp, Rectangle rect, Point start, Color c, int tolerance)
        {
            List<Point> points = new List<Point>();
            int startX = start.X;
            int startY = start.Y;
            int startR; int startG; int startB;
            int cR = c.R; int cG = c.G; int cB = c.B;
            bmp.GetPixel(startX, startY, out startR, out startG, out startB);
            if (IsTolerance(startR, startG, startB, cR, cG, cB, tolerance))
            {
                points.Add(start);
                return points;
            }
            int incrementX = startX + 1;
            int incrementY = startY + 1;
            int direction = 0; int length = 3;
            while ((incrementX < (rect.Width + 1)) || (incrementY < (rect.Height + 1) || (incrementX > -1) || (incrementY > -1)))
            {
                for (int go = 1; go < 5; go++)
                {
                    for (int i = 1; i < (length + 1); i++)
                    {
                        switch (direction)
                        {
                            case 0:
                                incrementY--;
                                break;
                            case 1:
                                incrementX--;
                                break;
                            case 2:
                                incrementY++;
                                break;
                            case 3:
                                incrementX++;
                                break;
                        }
                        if ((incrementX == -1) || (incrementY == -1))
                            if (points.Count == 0)
                                return null;
                            else
                                return points;
                        int foundR; int foundG; int foundB;
                        bmp.GetPixel(incrementX, incrementY, out foundR, out foundG, out foundB);
                        if (IsTolerance(foundR, foundG, foundB, cR, cG, cB, tolerance))
                            points.Add(new Point(incrementX, incrementY));
                    }
                    if (direction == 3)
                        direction = 0;
                    else
                        direction++;
                }
                incrementX++;
                incrementY++;
                length = length + 2;
            }
            if (points.Count == 0)
                return null;
            else
                return points;
        }

        /// <summary>
        /// Find a bitmap image.
        /// </summary>
        /// <param name="bmp">The bitmap to search in.</param>
        /// <param name="bitmapToFind">The bitmap to find.</param>
        /// <param name="rect">The bounds to search in (X, Y, X2, Y2).</param>
        /// <param name="mask">Skipped masked pixels with the colour Cyan (0,255,255).</param>
        /// <param name="tolerance">The tolerance of each pixel.</param>
        /// <returns>Returns the points of the found bitmap locations.</returns>
        public static List<Point> FindBitmap(BufferedImage bmp, BufferedImage bitmapToFind, Rectangle rect, bool mask, int tolerance)
        {
            List<Point> points = new List<Point>();
            int maskR = 0; int maskG = 255; int maskB = 255;
            int cR; int cG; int cB;
            bitmapToFind.GetPixel(0, 0, out cR, out cG, out cB);
            for (int Y = rect.Y; Y < (rect.Height + 1); Y++)
            {
                for (int X = rect.X; X < (rect.Width + 1); X++)
                {
                    int currentR; int currentG; int currentB;
                    bmp.GetPixel(X, Y, out currentR, out currentG, out currentB);
                    if (IsTolerance(cR, cG, cB, currentR, currentG, currentB, tolerance))
                    {
                        int xSave = X;
                        int ySave = Y;
                        for (int Y2 = 0; Y2 < bitmapToFind.imageHeight; Y2++)
                        {
                            for (int X2 = 0; X2 < bitmapToFind.imageWidth; X2++)
                            {
                                int checkR; int checkG; int checkB;
                                int check2R; int check2G; int check2B;
                                bitmapToFind.GetPixel(X2, Y2, out checkR, out checkG, out checkB);
                                bmp.GetPixel(X2 + xSave, Y2 + ySave, out check2R, out check2G, out check2B);
                                if (mask && IsMatch(checkR, checkG, checkB, maskR, maskG, maskB))
                                    goto End;
                                else if (!IsTolerance(checkR, checkG, checkB, check2R, check2G, check2B, tolerance))
                                    goto NextPixel;
                            End:
                                continue;
                            }
                        }
                        points.Add(new Point(xSave, ySave));
                    }
                NextPixel:
                    continue;
                }
            }
            if (points.Count == 0)
                return null;
            else
                return points;
        }

        /// <summary>
        /// Finds a masked bitmap regardless of the positioning of the mask.
        /// </summary>
        /// <param name="bmp">The bitmap to search in.</param>
        /// <param name="bitmapToFind">The bitmap to find.</param>
        /// <param name="rect">The bounds to search in (X, Y, X2, Y2).</param>
        /// <param name="mask">Colour of the masked pixels.</param>
        /// <param name="strict">If enabled, dropped pixels will not be skipped.</param>
        /// <returns>Returns the array of all the bitmap points found.</returns>
        public static List<Point> FindBitmapAdvanced(BufferedImage bmp, BufferedImage bitmapToFind, Rectangle rect, Color mask, bool strict)
        {
            int maskR = mask.R;
            int maskG = mask.G;
            int maskB = mask.B;
            int cR = -1;
            int cG = -1;
            int cB = -1;
            List<Point> points = new List<Point>();
            int foundX = 0; int foundY = 0;
            for (int colX = 0; colX < bitmapToFind.imageWidth; colX++)
            {
                for (int colY = 0; colY < bitmapToFind.imageHeight; colY++)
                {
                    bitmapToFind.GetPixel(colX, colY, out cR, out cG, out cB);
                    if (!IsMatch(cR, cG, cB, maskR, maskG, maskB))
                    {
                        foundX = colX;
                        foundY = colY;
                        goto Next;
                    }
                }
            }
            if (cR == -1)
                return null;
        Next:
            for (int Y = rect.Y; Y < (rect.Height + 1); Y++)
            {
                for (int X = rect.X; X < (rect.Width + 1); X++)
                {
                    int tempR; int tempG; int tempB;
                    bmp.GetPixel(X, Y, out tempR, out tempG, out tempB);
                    if (IsMatch(cR, cG, cB, tempR, tempG, tempB))
                    {
                        int xSave = X;
                        int ySave = Y;
                        for (int Y2 = 0; Y2 < bitmapToFind.imageHeight; Y2++)
                        {
                            for (int X2 = 0; X2 < bitmapToFind.imageWidth; X2++)
                            {
                                int checkR; int checkG; int checkB;
                                bitmapToFind.GetPixel(X2, Y2, out checkR, out checkG, out checkB);
                                int scaleX = (X2 + xSave) - foundX;
                                int scaleY = (Y2 + ySave) - foundY;
                                if (scaleX < 0 || scaleY < 0)
                                    goto NextPixel;
                                int check2R; int check2G; int check2B;
                                bmp.GetPixel(scaleX, scaleY, out check2R, out check2G, out check2B);
                                if (IsMatch(checkR, checkG, checkB, maskR, maskG, maskB))
                                {
                                    if (strict)
                                    {
                                        if (!IsMatch(check2R, check2G, check2B, 255, 255, 255))
                                            goto End;
                                    }
                                    else
                                        goto End;
                                }
                                if (!IsMatch(checkR, checkG, checkB, check2R, check2G, check2B))
                                    goto NextPixel;
                            End:
                                continue;
                            }
                        }
                        points.Add(new Point(xSave - foundX, ySave - foundY));
                    }
                NextPixel:
                    continue;
                }
            }
            if (points.Count != 0)
                return points;
            else
                return null;
        }

        /// <summary>
        /// Finds a masked bitmap regardless of the positioning of the mask.
        /// </summary>
        /// <param name="bmp">The bitmap to search in.</param>
        /// <param name="bitmapToFind">The bitmap to find.</param>
        /// <param name="chr">The character to append to the advanced point.</param>
        /// <param name="rect">The bounds to search in (X, Y, X2, Y2).</param>
        /// <param name="mask">Colour of the masked pixels.</param>
        /// <param name="strict">If enabled, dropped pixels will not be skipped.</param>
        /// <returns>Returns the array of all the bitmap points found.</returns>
        public static List<AdvancedPoint> FindBitmapAdvanced(BufferedImage bmp, BufferedImage bitmapToFind, char chr, Rectangle rect, Color mask, bool strict)
        {
            int maskR = mask.R;
            int maskG = mask.G;
            int maskB = mask.B;
            int cR = -1;
            int cG = -1;
            int cB = -1;
            List<AdvancedPoint> points = new List<AdvancedPoint>();
            int foundX = 0; int foundY = 0;
            for (int colX = 0; colX < bitmapToFind.imageWidth; colX++)
            {
                for (int colY = 0; colY < bitmapToFind.imageHeight; colY++)
                {
                    bitmapToFind.GetPixel(colX, colY, out cR, out cG, out cB);
                    if (!IsMatch(cR, cG, cB, maskR, maskG, maskB))
                    {
                        foundX = colX;
                        foundY = colY;
                        goto Next;
                    }
                }
            }
            if (cR == -1)
                return null;
        Next:
            for (int Y = rect.Y; Y < (rect.Height + 1); Y++)
            {
                for (int X = rect.X; X < (rect.Width + 1); X++)
                {
                    int tempR; int tempG; int tempB;
                    bmp.GetPixel(X, Y, out tempR, out tempG, out tempB);
                    if (IsMatch(cR, cG, cB, tempR, tempG, tempB))
                    {
                        int xSave = X;
                        int ySave = Y;
                        for (int Y2 = 0; Y2 < bitmapToFind.imageHeight; Y2++)
                        {
                            for (int X2 = 0; X2 < bitmapToFind.imageWidth; X2++)
                            {
                                int checkR; int checkG; int checkB;
                                bitmapToFind.GetPixel(X2, Y2, out checkR, out checkG, out checkB);
                                int scaleX = (X2 + xSave) - foundX;
                                int scaleY = (Y2 + ySave) - foundY;
                                if (scaleX < 0 || scaleY < 0)
                                    goto NextPixel;
                                int check2R; int check2G; int check2B;
                                bmp.GetPixel(scaleX, scaleY, out check2R, out check2G, out check2B);
                                if (IsMatch(checkR, checkG, checkB, maskR, maskG, maskB))
                                {
                                    if (strict)
                                    {
                                        if (!IsMatch(check2R, check2G, check2B, 255, 255, 255))
                                            goto End;
                                    }
                                    else
                                        goto End;
                                }
                                if (!IsMatch(checkR, checkG, checkB, check2R, check2G, check2B))
                                    goto NextPixel;
                            End:
                                continue;
                            }
                        }
                        points.Add(new AdvancedPoint(chr, xSave - foundX, ySave - foundY));
                    }
                NextPixel:
                    continue;
                }
            }
            if (points.Count != 0)
                return points;
            else
                return null;
        }
    }
}
