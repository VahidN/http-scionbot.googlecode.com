﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scion.Library.Antiban
{
    /// <summary>
    /// Generates a number which is used for a bot rest.
    /// </summary>
    public class SleepRandom
    {
        /// <summary>
        /// The random integer returned.
        /// </summary>
        public int random;
        /// <summary>
        /// Minimum value.
        /// </summary>
        public int minimumValue;
        /// <summary>
        /// Maximum value.
        /// </summary>
        public int maximumValue;

        /// <summary>
        /// Generates a number which is used for a bot rest.
        /// </summary>
        /// <param name="min">Minimum value.</param>
        /// <param name="max">Maximum value.</param>
        public SleepRandom(int min, int max)
        {
            minimumValue = min;
            maximumValue = max;
            random = new Random().Next(min, max);
        }
    }
}
