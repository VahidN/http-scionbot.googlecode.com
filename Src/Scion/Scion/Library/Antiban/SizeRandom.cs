﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scion.Library.Underlying;
using System.Drawing;

namespace Scion.Library.Antiban
{
    /// <summary>
    /// Gets a random point within the bounds of a bitmap.
    /// </summary>
    public class SizeRandom
    {
        /// <summary>
        /// The size.
        /// </summary>
        public Size size;
        /// <summary>
        /// The location to start at.
        /// </summary>
        public Point found;
        /// <summary>
        /// The random point generated.
        /// </summary>
        public Point random;

        /// <summary>
        /// Gets a random point within the bounds of a bitmap.
        /// </summary>
        /// <param name="bmpSize">The size.</param>
        /// <param name="fnd">The location to start at. Can be null.</param>
        public SizeRandom(Size sz, Point fnd)
        {
            found = fnd;
            size = sz;
            random = GetRandom();
        }

        private Point GetRandom()
        {
            int x = new Random().Next(0, size.Width);
            int y = new Random().Next(0, size.Height);
            if (found == null)
                return new Point(x, y);
            else
                return new Point(x + found.X, y + found.Y);
        }
    }
}
