﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scion.Library.Antiban
{
    /// <summary>
    /// Generates an interval that is used for breaks in-between key presses.
    /// </summary>
    public class TypeRandom
    {
        /// <summary>
        /// The type interval returned.
        /// </summary>
        public int random;
        /// <summary>
        /// The base interval value.
        /// </summary>
        public int intervalValue;
        /// <summary>
        /// The maximum value to append.
        /// </summary>
        public int maximumValue;

        /// <summary>
        /// Generates an interval that is used for breaks in-between key presses.
        /// </summary>
        /// <param name="interval">The base interval value.</param>
        /// <param name="maxValue">The maximum value to append.</param>
        public TypeRandom(int interval, int maxValue)
        {
            intervalValue = interval;
            maximumValue = maxValue;
            int rnd = new Random().Next(0, maxValue);
            random = interval + NPRandom.Determine(rnd);
        }
    }
}
