﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scion.Library.Antiban
{
    /// <summary>
    /// Negative-Positive Random Generator. Used for antiban.
    /// </summary>
    public class NPRandom
    {
        /// <summary>
        /// Makes the value argument either negative or positive.
        /// </summary>
        /// <param name="value">An integer of the number you want to make negative or positive.</param>
        /// <returns>Returns an integer either negative or positive.</returns>
        public static int Determine(int value)
        {
            System.Threading.Thread.Sleep(5);
            int yesNo = new Random().Next(2);
            if (yesNo == 0)
            {
                return (value - (value * 2));
            }
            else
            {
                return value;
            }
        }
    }
}
