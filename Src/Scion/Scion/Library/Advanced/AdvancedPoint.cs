﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Scion.Library.Advanced
{
    /// <summary>
    /// The class basically wraps a character with a X/Y coordinate point.
    /// </summary>
    public class AdvancedPoint
    {
        public int x;
        public int y;
        public char chr;
        public Point xy;


        public AdvancedPoint(char c, int xax, int yax)
        {
            x = xax;
            y = yax;
            chr = c;
            xy = new Point(xax, yax);
        }
    }
}
