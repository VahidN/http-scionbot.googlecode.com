﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scion.Library.Underlying;
using Scion.Misc;
using System.IO;
using System.Drawing;
namespace Scion.Library.Advanced
{
    /// <summary>
    /// This class stores all the characters loaded by Scion when Client.cs is executed.
    /// </summary>
    public class Fonts
    {
        /// <summary>
        /// The NPC characters.
        /// </summary>
        public static List<BitmapChar> CharsNPC = new List<BitmapChar>();
        /// <summary>
        /// The characters used in trades.
        /// </summary>
        public static List<BitmapChar> CharsTrade = new List<BitmapChar>();
        /// <summary>
        /// The smaller characters.
        /// </summary>
        public static List<BitmapChar> SmallChars = new List<BitmapChar>();
        /// <summary>
        /// The player statistics characters.
        /// </summary>
        public static List<BitmapChar> StatChars = new List<BitmapChar>();
        /// <summary>
        /// The larger characters.
        /// </summary>
        public static List<BitmapChar> UpChars = new List<BitmapChar>();

        /// <summary>
        /// Loads all fonts into memory.
        /// </summary>
        public static void LoadAllFonts()
        {
            string directory = ScionSettings.StartupPath + @"\Fonts";
            string[] paths = new string[] {"CharsNPC", "CharsTrade", "SmallChars", "StatChars", "UpChars"};
            foreach (string path in paths)
            {
                DirectoryInfo di = new DirectoryInfo(directory + @"\" + path);
                foreach (FileInfo file in di.GetFiles())
                {
                    if (file.Extension.IndexOf("bmp") > -1)
                    {
                        switch (path)
                        {
                            case "CharsNPC":
                                CharsNPC.Add(new BitmapChar(new BufferedImage(new Bitmap(file.FullName)), Convert.ToInt32(file.Name.Remove(file.Name.IndexOf(".")))));
                                break;
                            case "CharsTrade":
                                CharsTrade.Add(new BitmapChar(new BufferedImage(new Bitmap(file.FullName)), Convert.ToInt32(file.Name.Remove(file.Name.IndexOf(".")))));
                                break;
                            case "SmallChars":
                                SmallChars.Add(new BitmapChar(new BufferedImage(new Bitmap(file.FullName)), Convert.ToInt32(file.Name.Remove(file.Name.IndexOf(".")))));
                                break;
                            case "StatChars":
                                StatChars.Add(new BitmapChar(new BufferedImage(new Bitmap(file.FullName)), Convert.ToInt32(file.Name.Remove(file.Name.IndexOf(".")))));
                                break;
                            case "UpChars":
                                UpChars.Add(new BitmapChar(new BufferedImage(new Bitmap(file.FullName)), Convert.ToInt32(file.Name.Remove(file.Name.IndexOf(".")))));
                                break;
                        }
                    }
                }
            }
        }
    }
}
