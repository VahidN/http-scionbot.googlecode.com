﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections;

namespace Scion.Library.Advanced
{
    /// <summary>
    /// This class is used to sort a cluster of points in numeric order.
    /// </summary>
    public class PointSort : IComparer<AdvancedPoint>
    {
        public enum Mode
        {
            X,
            Y
        }

        Mode currentMode = Mode.X;

        public PointSort(Mode mode)
        {
            currentMode = mode;
        }

        //Comparing function
        //Returns one of three values - 0 (equal), 1 (greater than), 2 (less than)
        int IComparer<AdvancedPoint>.Compare(AdvancedPoint a, AdvancedPoint b)
        {
            AdvancedPoint point1 = a;
            AdvancedPoint point2 = b;

            if (currentMode == Mode.X) //Compare X values
            {
                if (point1.x > point2.x)
                    return 1;
                else if (point1.x < point2.x)
                    return -1;
                else
                    return 0;
            }
            else
            {
                if (point1.y > point2.y) //Compare Y Values
                    return 1;
                else if (point1.y < point2.y)
                    return -1;
                else
                    return 0;
            }
        }
    }
}
