﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Scion.Library
{
    /// <summary>
    /// An object to be drawn onto the canvas.
    /// </summary>
    public class PaintObject
    {
        /// <summary>
        /// Get/Set the string painted to the canvas.
        /// </summary>
        public string text;
        /// <summary>
        /// Get/Set the position the object is painted onto the canvas.
        /// </summary>
        public Point position;
        /// <summary>
        /// Get/Set the image that is painted onto the canvas.
        /// </summary>
        public Bitmap image;
        /// <summary>
        /// Get/Set the rectangle that is painted onto the canvas.
        /// </summary>
        public Rectangle rect;
        /// <summary>
        /// Get/Set if the object created is filled with a solid colour or not.
        /// </summary>
        public bool full;
        /// <summary>
        /// Get/Set the colour of brush that is used.
        /// </summary>
        public Brush brush;
        /// <summary>
        /// Get/Set the type of object that is getting painted to the canvas.
        /// </summary>
        public int type;
        /// <summary>
        /// Get/Set the colour of the pen that is used.
        /// </summary>
        public Pen pen;
        /// <summary>
        /// Get/Set the second point of where the object is painted onto the canvas.
        /// </summary>
        public Point position2;
        private bool disposed = false;

        /// <summary>
        /// Dispose of all the resources used by PaintObject.
        /// </summary>
        public void Dispose()
        {
            try { image.Dispose(); }
            catch { }
            try { brush.Dispose(); }
            catch { }
            try { pen.Dispose(); }
            catch { }
            disposed = true;
        }

        /// <summary>
        /// Get if the PaintObject is disposed.
        /// </summary>
        public bool IsDiposed
        {
            get
            {
                return disposed;
            }
        }

        /// <summary>
        /// Draw text onto the canvas.
        /// </summary>
        /// <param name="str">The string to paint to the canvas.</param>
        /// <param name="b">The colour of the brush.</param>
        /// <param name="pos">The position of where to draw the text.</param>
        public PaintObject(string str, Brush b, Point pos)
        {
            type = (int)PaintType.Text;
            text = str;
            brush = b;
            position = pos;
        }

        /// <summary>
        /// Draw an image onto the canvas.
        /// </summary>
        /// <param name="img">The image to paint.</param>
        /// <param name="pos">The position of where to draw the image.</param>
        public PaintObject(Bitmap img, Point pos)
        {
            type = (int)PaintType.Image;
            image = img;
            position = pos;
        }

        /// <summary>
        /// Draw a rectangle onto the canvas.
        /// </summary>
        /// <param name="b">The colour of the brush.</param>
        /// <param name="r">The dimensions of the rectangle. Includes position to be drawn at.</param>
        /// <param name="f">Draw a filled rectangle (true), or a "bordered" rectangle (false).</param>
        public PaintObject(Brush b, Rectangle r, bool f)
        {
            type = (int)PaintType.Rectangle;
            brush = b;
            rect = r;
            full = f;
        }

        /// <summary>
        /// Draw a line onto the canvas.
        /// </summary>
        /// <param name="p">The colour of the pen.</param>
        /// <param name="pos">Start position of the line.</param>
        /// <param name="pos2">End position of the line.</param>
        public PaintObject(Pen p, Point pos, Point pos2)
        {
            type = (int)PaintType.Line;
            pen = p;
            position = pos;
            position2 = pos2;
        }

        /// <summary>
        /// Draw an ellipse onto the canvas.
        /// </summary>
        /// <param name="p">The colour of the pen.</param>
        /// <param name="r">The dimensions of the ellipse. Includes start position.</param>
        /// <param name="f">Draw a filled ellipse (true), or a "bordered" ellipse (false).</param>
        public PaintObject(Pen p, Rectangle r, bool f)
        {
            type = (int)PaintType.Ellipse;
            pen = p;
            rect = r;
            full = f;
        }

        /// <summary>
        /// The PaintType of the PaintObject. Compared with PaintObject.type.
        /// </summary>
        public enum PaintType
        {
            Text = 1,
            Image = 2,
            Rectangle = 3,
            Line = 4,
            Ellipse = 5,
        }
    }
}
