﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scion.Library.RS
{
    /// <summary>
    /// Stores the coordinates of a tile.
    /// </summary>
    public class Tile
    {
        /// <summary>
        /// The X-axis of the tile.
        /// </summary>
        public int x;
        /// <summary>
        /// The Y-axis of the tile.
        /// </summary>
        public int y;

        /// <summary>
        /// Stores the coordinates of a tile.
        /// </summary>
        /// <param name="_x">The X coordinate.</param>
        /// <param name="_y">The Y coordinate.</param>
        public Tile(int _x, int _y)
        {
            x = _x;
            y = _y;
        }
    }
}
