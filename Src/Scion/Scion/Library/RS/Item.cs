﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scion.Library.Underlying;
using System.Net;
using System.Drawing;

namespace Scion.Library.RS
{
    /// <summary>
    /// Any item that can be searched with a 2D bitmap.
    /// </summary>
    public class Item
    {
        public string name;
        public int id;
        Superior super;
        public BufferedImage image;

        public Item(Superior s, int itemID)
        {
            id = itemID;
            super = s;
            image = GetImage(itemID);
        }

        public Item(Superior s, string itemName)
        {
            name = itemName;
            super = s;
            image = GetImage(itemName);
        }

        private BufferedImage GetImage(int itemID)
        {
            WebRequest wr = WebRequest.Create("http://www.scionbot.com/Scion/items/" + itemID + ".png");
            WebResponse ws = wr.GetResponse();
            Bitmap bmp;
            try
            {
                bmp = (Bitmap)Image.FromStream(ws.GetResponseStream());
            }
            catch
            {
                return null;
            }
            if (bmp == null) return null;
            return new BufferedImage(bmp);
        }

        private BufferedImage GetImage(string itemName)
        {
            WebClient wc = new WebClient();
            string src = wc.DownloadString("http://www.scionbot.com/Scion/items/index.txt");
            int index = src.IndexOf(itemName) + itemName.Length + "&id=".Length;
            string id = src.Remove(0, index);
            int iD = int.Parse(id.Replace('#', ' ').Trim());
            WebRequest wr = WebRequest.Create("http://www.scionbot.com/Scion/items/" + iD + ".png");
            WebResponse ws = wr.GetResponse();
            Bitmap bmp;
            try
            {
                bmp = (Bitmap)Image.FromStream(ws.GetResponseStream());
            }
            catch
            {
                return null;
            }
            if (bmp == null) return null;
            return new BufferedImage(bmp);
        }
    }
}
