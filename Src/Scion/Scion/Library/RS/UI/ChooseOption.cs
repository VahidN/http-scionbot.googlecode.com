﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Scion.Library.Advanced;
using Scion.Library.Underlying;
using Scion.Library.Antiban;

namespace Scion.Library.RS.UI
{
    /// <summary>
    /// Clicks an action within the bounds of the context menu.
    /// </summary>
    public class ChooseOption : Determine
    {
        Superior super;

        /// <summary>
        /// All the colours the text can be.
        /// </summary>
        public static Color[] Common = new Color[] { Color.FromArgb(255, 255, 255), Color.FromArgb(255, 0, 0), Color.FromArgb(255, 176, 0), Color.FromArgb(255, 255, 0), Color.FromArgb(192, 255, 0), Color.FromArgb(0, 255, 255) };
        private static BufferedImage optionCorner;
        private static BufferedImage optionBottomCorner;

        public static void DecryptImages()
        {
            optionCorner = BitmapString.Decrypt("(7,6)|93,84,71|93,84,71|93,84,71|93,84,71|93,84,71|93,84,71|93,84,71|93,84,71|0,0,0|0,0,0|0,0,0|0,0,0|0,0,0|0,0,0|93,84,71|0,0,0|0,0,0|0,0,0|0,0,0|0,0,0|0,0,0|93,84,71|0,0,0|0,0,0|0,0,0|0,0,0|0,0,0|0,0,0|93,84,71|0,0,0|0,0,0|0,0,0|0,0,0|0,0,0|93,84,71|93,84,71|0,0,0|0,0,0|0,0,0|0,0,0|93,84,71|93,84,71|", true);
            optionBottomCorner = BitmapString.Decrypt("(6,5)|93,84,71|93,84,71|93,84,71|93,84,71|0,0,0|93,84,71|93,84,71|93,84,71|93,84,71|93,84,71|0,0,0|93,84,71|93,84,71|93,84,71|93,84,71|93,84,71|0,0,0|93,84,71|93,84,71|93,84,71|93,84,71|93,84,71|0,0,0|93,84,71|0,0,0|0,0,0|0,0,0|0,0,0|0,0,0|93,84,71|", true);
        }

        /// <summary>
        /// Clicks an action within the bounds of the context menu.
        /// </summary>
        /// <param name="s">An instance of the Superior class allocated to the bot's stub.</param>
        public ChooseOption(Superior s)
        {
            super = s;
        }

        /// <summary>
        /// Clicks an action.
        /// </summary>
        /// <param name="textColour">The colour of the text.</param>
        /// <param name="action">The action to click.</param>
        /// <returns>Returns a boolean indicating if the function succeeded.</returns>
        public bool DoAction(Color[] textColour, string action)
        {
            Point p = super.FindBitmap(ChooseOption.optionCorner, new Rectangle(0, 0, 755, 495), false, 10);
            if (Decide(p))
            {
                Point p2 = super.FindBitmap(ChooseOption.optionBottomCorner, new Rectangle(p.X, p.Y, 755, 495), false, 10);
                if (Decide(p2))
                {
                    PointRandom pr = super.ClickTextStrict(new Rectangle(p.X, p.Y, p2.X + ChooseOption.optionBottomCorner.imageWidth, p2.Y + ChooseOption.optionBottomCorner.imageHeight), action, CharFont.UpChars, textColour, 5, 10);
                    if (Decide(pr))
                    {
                        super.Mouse.MoveClick(pr, true);
                        return true;
                    }
                }
                else
                {
                    PointRandom pr = super.ClickTextStrict(new Rectangle(p.X, p.Y, 765 - ChooseOption.optionBottomCorner.imageWidth, 503 - ChooseOption.optionBottomCorner.imageHeight), action, CharFont.UpChars, textColour, 5, 10);
                    if (Decide(pr))
                    {
                        super.Mouse.MoveClick(pr, true);
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
