﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Scion.Library.Advanced;
using Scion.Library.Underlying;

namespace Scion.Library.RS.UI
{
    /// <summary>
    /// Finds/Gets text in the chatbox.
    /// </summary>
    public class ChatText : Determine
    {
        Superior super;

        public static Rectangle Line1 = new Rectangle(8, 442, 495, 460);
        public static Rectangle Line2 = new Rectangle(8, 428, 495, 446);
        public static Rectangle Line3 = new Rectangle(8, 414, 495, 432);
        public static Rectangle Line4 = new Rectangle(8, 400, 495, 418);
        public static Rectangle Line5 = new Rectangle(8, 386, 495, 404);
        public static Rectangle Line6 = new Rectangle(8, 372, 495, 390);
        public static Rectangle Line7 = new Rectangle(8, 358, 495, 376);
        public static Rectangle Line8 = new Rectangle(8, 344, 495, 362);
        /// <summary>
        /// The bounds of the chat screen.
        /// </summary>
        public static Rectangle Chatbox = Areas.ChatScreenBounds;

        /// <summary>
        /// The common colours of the chatbox.
        /// </summary>
        public static Color[] Common = new Color[] { Color.FromArgb(0,0,0), Color.FromArgb(0,0,255), Color.FromArgb(128,0,128), Color.FromArgb(128,0,0) };

        /// <summary>
        /// Finds/Gets text in the chatbox.
        /// </summary>
        public ChatText(Superior s)
        {
            super = s;
        }

        /// <summary>
        /// Gets a single line of text.
        /// </summary>
        /// <param name="line">The bounds of the line to get.</param>
        /// <returns>Returns a string of the text found.</returns>
        public string GetLine(Rectangle line)
        {
            return OCR.GetText(super.Canvas.GetCanvas(0, true), line, CharFont.SmallChars, Common, 5, false);
        }

        /// <summary>
        /// Finds text in the area specified.
        /// </summary>
        /// <param name="area">The area to search in.</param>
        /// <param name="str">The text to find.</param>
        /// <returns>Returns true if the text specified was found.</returns>
        public bool FindText(Rectangle area, string str)
        {
            return Decide(OCR.FindText(super.Canvas.GetCanvas(0, true), area, str, CharFont.SmallChars, Common, 6, 5, true));
        }

        /// <summary>
        /// Finds any type of text with a different font in the area specified.
        /// </summary>
        /// <param name="area">The area to search in.</param>
        /// <param name="font">The font of the text.</param>
        /// <param name="str">The text t find.</param>
        /// <returns>Returns true if the text specified was found.</returns>
        public bool FindText(Rectangle area, CharFont font, string str)
        {
            return Decide(OCR.FindText(super.Canvas.GetCanvas(0, true), area, str, font, Common, 6, 5, true));
        }
    }
}
