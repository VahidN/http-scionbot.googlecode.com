﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scion.Library.Underlying;
using System.Drawing;
using Scion.Library.Antiban;

namespace Scion.Library.RS.UI
{
    /// <summary>
    /// Contains tab functions.
    /// </summary>
    public class Tab : Determine
    {
        Superior super;

        /// <summary>
        /// Contains tab functions.
        /// </summary>
        /// <param name="s">The bot's instance.</param>
        public Tab(Superior s)
        {
            super = s;
        }

        /// <summary>
        /// Gets the current tab.
        /// </summary>
        public int GetCurrentTab()
        {
            BufferedImage bi = super.Canvas.GetCanvas(0, true);
            bool val = false;
            for (int i = 1; i < 16; i++)
            {
                switch (i)
                {
                    case 1:
                        val = IsMatch(bi.GetPixel(543, 178), super.FromRGB(90, 30, 24));
                        break;
                    case 2:
                        val = IsMatch(bi.GetPixel(570, 179), super.FromRGB(107, 36, 27));
                        break;
                    case 3:
                        val = IsMatch(bi.GetPixel(604, 174), super.FromRGB(113, 38, 29));
                        break;
                    case 4:
                        val = IsMatch(bi.GetPixel(632, 172), super.FromRGB(113, 38, 29));
                        break;
                    case 5:
                        val = IsMatch(bi.GetPixel(666, 182), super.FromRGB(100, 34, 25));
                        break;
                    case 6:
                        val = IsMatch(bi.GetPixel(699, 177), super.FromRGB(113, 38, 29));
                        break;
                    case 7:
                        val = IsMatch(bi.GetPixel(735, 177), super.FromRGB(107, 36, 27));
                        break;
                    case 8:
                        val = IsMatch(bi.GetPixel(569, 471), super.FromRGB(113, 38, 29));
                        break;
                    case 9:
                        val = IsMatch(bi.GetPixel(598, 471), super.FromRGB(113, 38, 29));
                        break;
                    case 10:
                        val = IsMatch(bi.GetPixel(633, 478), super.FromRGB(107, 36, 27));
                        break;
                    case 11:
                        val = IsMatch(bi.GetPixel(668, 476), super.FromRGB(107, 36, 27));
                        break;
                    case 12:
                        val = IsMatch(bi.GetPixel(703, 481), super.FromRGB(90, 30, 24));
                        break;
                    case 13:
                        val = IsMatch(bi.GetPixel(740, 471), super.FromRGB(107, 36, 27));
                        break;
                    case 14:
                        val = IsMatch(bi.GetPixel(555, 275), super.FromRGB(255, 152, 31));
                        break;
                    case 15:
                        val = IsMatch(bi.GetPixel(533, 475), super.FromRGB(94, 32, 24));
                        break;
                }
                if (val) return i;
            }
            return -1;
        }

        /// <summary>
        /// Switches the tab.
        /// </summary>
        /// <param name="tab">The tab to switch to.</param>
        /// <returns>Returns true if the function succeeded.</returns>
        public bool SwitchTab(int tab)
        {
            PointRandom pr;
            if (tab < 1 || tab > 15) return false;
            if (tab == 14)
                pr = new PointRandom(752, 12, 8);
            else if (tab == 15)
                pr = new PointRandom(538, 485, 8);
            else if (tab <= 7)
                pr = new PointRandom(540 + ((tab - 1) * 33), 185, 8);
            else
                pr = new PointRandom(540 + ((tab % 7) * 33), 485, 8);
            int count = 0;
            while ((GetCurrentTab() != tab) && (count < 4))
            {
                super.Mouse.MoveClick(pr, true);
                count++;
                super.Sleep(new SleepRandom(300, 400));
            }
            return GetCurrentTab() == tab;
        }

    }
}
