﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Scion.Library.Advanced;
using Scion.Library.Underlying;

namespace Scion.Library.RS.UI
{
    /// <summary>
    /// Find/Gets the top text string.
    /// </summary>
    public class TopText : Determine
    {
        Superior super;

        /// <summary>
        /// The white colour for white top text.
        /// </summary>
        public static Color[] White = { Color.FromArgb(224, 226, 226), Color.FromArgb(201, 201, 201) };
        /// <summary>
        /// The neon-yellow colour for neon-yellow top text.
        /// </summary>
        public static Color[] NeonYellow = { Color.FromArgb(201, 201, 6), Color.FromArgb(224, 223, 26) };
        /// <summary>
        /// The green colour for green top text.
        /// </summary>
        public static Color[] Green = { Color.FromArgb(151, 199, 2), Color.FromArgb(171, 218, 11) };
        /// <summary>
        /// The red colour for red top text.
        /// </summary>
        public static Color[] Red = { Color.FromArgb(229, 17, 8), Color.FromArgb(214, 2, 1) };
        /// <summary>
        /// The yellow colour for yellow top text.
        /// </summary>
        public static Color[] Yellow = { Color.FromArgb(238, 236, 8), Color.FromArgb(223, 223, 0) };
        /// <summary>
        /// The cyan colour for cyan top text.
        /// </summary>
        public static Color[] Cyan = { Color.FromArgb(9, 225, 224), Color.FromArgb(6, 200, 200), Color.FromArgb(44, 237, 238)};
        /// <summary>
        /// The neon-green colour for neon-green top text.
        /// </summary>
        public static Color[] NeonGreen = { Color.FromArgb(28, 225, 5) };
        /// <summary>
        /// The brown colour for brown top text.
        /// </summary>
        public static Color[] Brown = { Color.FromArgb(229, 142, 81), Color.FromArgb(200, 116, 55) };
        /// <summary>
        /// The tolerance of each pixel compared.
        /// </summary>
        public static int Tolerance = 40;
        /// <summary>
        /// A cluster of all the colours used in the top text.
        /// </summary>
        public static Color[] Common;

        /// <summary>
        /// Dump all the toptext colours into common.
        /// </summary>
        public static void PopulateCommon()
        {
            Common = DumpArrays(new Array[] { White, NeonYellow, Green, Red, Yellow, Cyan, NeonGreen, Brown });
        }

        private static Color[] DumpArrays(Array[] colours)
        {
            int totalLength = 0;
            foreach (Array r in colours)
                totalLength += r.Length;
            Color[] master = new Color[totalLength];
            int index = 0;
            foreach (Array r in colours)
            {
                r.CopyTo(master, index);
                index += r.Length;
            }
            return master;
        }

        /// <summary>
        /// Find/Gets the top text string.
        /// </summary>
        /// <param name="s">An instance of the Superior class allocated to the bot's stub.</param>
        public TopText(Superior s)
        {
            super = s;
        }

        /// <summary>
        /// Gets everything in the top text within the colour boundries.
        /// </summary>
        /// <param name="textColours">The colour of the text to get.</param>
        /// <param name="tolerance">The tolerance of each pixel compared.</param>
        /// <param name="basic">If true, the result returned will only contain numbers and letters.</param>
        /// <returns>Returns a string of characters the function found within the bounds.</returns>
        public string GetText(Color[] textColours, int tolerance, bool basic)
        {
            return OCR.GetText(super.Stub.botCanvas.GetCanvas(0, true), new Rectangle(0, 0, 515, 26), CharFont.UpChars, textColours, tolerance, basic);
        }

        /// <summary>
        /// Finds a string of text within the top text boundries.
        /// </summary>
        /// <param name="textColours">The colour of the text to get.</param>
        /// <param name="text">The text to find.</param>
        /// <param name="tolerance">The tolerance of each pixel compared.</param>
        /// <returns>Returns a boolean indicating if the function succeeded in finding the text.</returns>
        public bool FindText(Color[] textColours, string text, int tolerance)
        {
            return Decide(OCR.FindText(super.Stub.botCanvas.GetCanvas(0, true), new Rectangle(0, 0, 515, 26), text, CharFont.UpChars, textColours, 30, tolerance, false));
        }
    }
}
