﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scion;
using Scion.Library.Antiban;
using System.Threading;
using System.Drawing;
using Scion.Library.Advanced;
using Scion.Library.Underlying;
using Scion.Library.RS;
using Scion.Core;

namespace Scion.Library.RS
{
    public class FailSafe : Superior
    {
        public FailSafe(Stub s)
        {
            baseStub = s;
        }

        public bool CheckMissingTabs()
        {
            if (!IsLoggedIn())
                return false;
            BufferedImage bi = Canvas.GetCanvas(0, true);
            for (int x = 730; x < 753; x++)
            {
                for (int y = 175; y < 197; y++)
                {
                    if (Determine.IsTolerance(bi.GetPixel(x, y), FromRGB(0, 0, 1), 2))
                        return false;
                }
            }
            return true;
        }

        public void SolveMissingTabs()
        {
            Log.WriteLine("Some tabs are missing in the user interface. We're most likely in a random or lost.");
            if (IsLoggedIn())
            {
                Interface.LogOut();
                Sleep(new SleepRandom(300, 400));
            }
            Script.Terminate();
        }

        public bool CheckConnectionLost()
        {
            if (ConnectionLost())
                return true;
            return false;
        }

        public void SolveConnectionLost()
        {
            Log.WriteLine("A connection failure has occured.");
            Script.Terminate();
        }
    }
}
