﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Scion.Library
{
    /// <summary>
    /// Areas of the RS client.
    /// </summary>
    public class Areas
    {
        /// <summary>
        /// The Gamesize of the entire RS Canvas.
        /// </summary>
        public static Size GameSize = new Size(765, 503);

        /// <summary>
        /// The centre point of the character on the canvas.
        /// </summary>
        public static Point CharacterCentre = new Point(244, 157);

        /// <summary>
        /// The 3D gameplay bounds.
        /// </summary>
        public static Rectangle WorldBounds = new Rectangle(5, 5, 512, 334);

        /// <summary>
        /// The bounds of the inventory.
        /// </summary>
        public static Rectangle InventoryBounds = new Rectangle(547, 202, 737, 466);

        /// <summary>
        /// The bounds of the chatscreen.
        /// </summary>
        public static Rectangle ChatScreenBounds = new Rectangle(4, 342, 496, 460);
    }
}
