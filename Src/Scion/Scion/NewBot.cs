﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Scion.Properties;
using Scion.Misc;
using Scion.Core;

namespace Scion
{
    /// <summary>
    /// The New Bot form.
    /// </summary>
    public partial class NewBot : Form
    {
        private Client client;

        public NewBot(Client c)
        {
            client = c;
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AccountManager am = new AccountManager();
            am.MdiParent = client;
            am.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex < 0)
            {
                MessageBox.Show("Please select an account.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int world;
            try
            {
                world = int.Parse(textBox1.Text);
            }
            catch
            {
                MessageBox.Show("Invalid world number!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                string data = Scion.Properties.Settings.Default.Accounts[comboBox1.SelectedIndex].ToString();
                string password = AccountCrypt.DecryptPassword(data);
                string username = AccountCrypt.DecryptUsername(data);
                Account account = new Account(username, password, world, checkBox1.Checked);
                int botPort;
                if (client.BotList.Count() == 0)
                    botPort = 10001;
                else
                    botPort = client.BotList[client.BotList.Count() - 1].port + 1;
                Bot bot = new Bot(client, client.BotList.Count, botPort, account);
                bot.MdiParent = client;
                bot.ClientSize = new Size(765, 503);
                ToolStripMenuItem item = new ToolStripMenuItem("Bot " + client.BotList.Count);
                item.Click += new EventHandler(client.focusClicked);
                client.BotList.Add(bot);
                bot.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to make bot. Message: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                GC.Collect();
            }
            this.Close();
        }

        private void NewBot_Load(object sender, EventArgs e)
        {
            foreach (string str in Scion.Properties.Settings.Default.Accounts)
            {
                comboBox1.Items.Add(AccountCrypt.DecryptUsername(str));
            }
        }
    }
}
