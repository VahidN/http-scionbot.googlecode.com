﻿//Copyright (C) 2009  Jaco (ScionBot.com)

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Scion.Library.Underlying;
using Scion.Misc;

namespace Scion
{
    /// <summary>
    /// This class basically displays a form that allows developers to develop at a much faster rate.
    /// </summary>
    public partial class Developer : Form
    {
        BufferedImage canvas;

        public Developer(BufferedImage c)
        {
            canvas = c;
            InitializeComponent();
        }

        private void Developer_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = canvas.image;
            this.pictureBox1.MouseMove += new MouseEventHandler(pictureBox1_MouseMove);
            this.FormClosing += new FormClosingEventHandler(Developer_FormClosing);
            this.KeyPress += new KeyPressEventHandler(Developer_KeyPress);
        }

        private void Developer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'z')
                Clipboard.SetText(label2.Text);
            else if (e.KeyChar == 'x')
                Clipboard.SetText(label4.Text);
        }

        private void Developer_FormClosing(object sender, FormClosingEventArgs e)
        {
            canvas.image.Dispose();
            GC.Collect();
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            label2.Text = e.X.ToString() + ", " + e.Y.ToString();
            Color c = canvas.GetPixel(e.X, e.Y);
            label4.Text = c.R.ToString() + ", " + c.G.ToString() + ", " + c.B.ToString();
            panel1.BackColor = c;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "PNG File (*.png)|*.png|BMP File (*.bmp)|*.bmp|JPG File (*.jpg)|*.jpg";
                DialogResult dr = ofd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    Bitmap bmp = (Bitmap)Bitmap.FromFile(ofd.FileName);
                    Clipboard.SetText(BitmapString.Encrypt(new BufferedImage(bmp), true));
                    bmp.Dispose();
                    MessageBox.Show("The image's text has been copied to your clipboard.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                ofd.Dispose();
                GC.Collect();
            }
            catch
            {
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Insert something into the text box first.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                Bitmap bmp = BitmapString.Decrypt(textBox1.Text);
                ImageDisplay id = new ImageDisplay(new BufferedImage(bmp));
                id.Show();
                GC.Collect();
            }
            catch
            {
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "JPG File (*.jpg)|*.jpg|BMP File (*.bmp)|*.bmp";
                DialogResult dr = sfd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    if (sfd.FilterIndex == 0)
                    {
                        canvas.image.Save(sfd.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    else
                    {
                        canvas.image.Save(sfd.FileName, System.Drawing.Imaging.ImageFormat.Bmp);
                    }
                }
                sfd.Dispose();
                GC.Collect();
            }
            catch
            {
            }
        }
    }
}
